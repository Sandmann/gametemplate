﻿namespace Assets.Scripts.Dungeon
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Combat;
    using Infrastructure.Messaging;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class BattlefieldController : MonoBehaviour
    {
        public SpriteRenderer EnemyPortrait;
        public List<GameObject> EnemyTemplates = new List<GameObject>();
        
        public Transform CharacterContainer;
        public Vector3 LeftSpawnPosition = new Vector3(-8.0f, 0.0f, 0.0f);
        public Vector3 RightSpawnPosition = new Vector3(8.0f, 0.0f, 0.0f);

        private List<EnemyAvatarController> _enemies;

        protected void Start()
        {
            _enemies = new List<EnemyAvatarController>();

            foreach (var enemy in EnemyTemplates)
            {
                enemy.CreatePool();
            }         

            Messenger.Instance.Subscribe<SelectEnemyMessage>(SelectEnemy);
            Messenger.Instance.Subscribe<LastEnemyDestroyedMessage>(DestroyLastEnemy);
            Messenger.Instance.Subscribe<CreatedEnemyBlockMessage>(CreateEnemyBlock);
            Messenger.Instance.Subscribe<DestroyEnemyAvatarMessage>(DestroyEnemy);
        }

        protected void OnDestroy()
        {
            Messenger.Instance.Unsubscribe<SelectEnemyMessage>(SelectEnemy);
            Messenger.Instance.Unsubscribe<LastEnemyDestroyedMessage>(DestroyLastEnemy);
            Messenger.Instance.Unsubscribe<CreatedEnemyBlockMessage>(CreateEnemyBlock);
            Messenger.Instance.Unsubscribe<DestroyEnemyAvatarMessage>(DestroyEnemy);
        }

        private void DestroyEnemy(DestroyEnemyAvatarMessage msg)
        {
            DestroyEnemy(msg.EnemyAvatar);
        }

        private void CreateEnemyBlock(CreatedEnemyBlockMessage msg)
        {
            CreateEnemy(msg.Item);
        }
        private void DestroyLastEnemy(LastEnemyDestroyedMessage msg)
        {
            EnemyPortrait.sprite = null;
            Messenger.Instance.Publish(new EnemyUpdatesHealthMessage() { Amount = 0 });
        }

        private void SelectEnemy(SelectEnemyMessage msg)
        {
            EnemyPortrait.sprite = msg.Enemy.Portrait;

            Messenger.Instance.Publish(new EnemyUpdatesHealthMessage() { Amount = msg.Enemy.CurrentHealth });
        }

        private void CreateEnemy(GameObject block = null)
        {
            Vector3 target;
            if (_enemies.All(x => x.transform.localPosition != RightSpawnPosition))
            {
                target = RightSpawnPosition;
            }
            else if (_enemies.All(x => x.transform.localPosition != LeftSpawnPosition))
            {
                target = LeftSpawnPosition;
            }
            else
            {
                return;
            }

            GameObject obj = null;
            if(block != null)
            {
                obj = EnemyTemplates.FirstOrDefault(e => block.GetComponent<BlockController>().Info.
                                                    BlockType.Equals(e.GetComponent<EnemyAvatarController>().Description.Name));
            }
            else
            {
                obj = EnemyTemplates[Random.Range(0, EnemyTemplates.Count)];
            }
                
            var instance = obj.Spawn(CharacterContainer);
            if (instance)
            {
//                instance.transform.SetParent(CharacterContainer);
                instance.transform.localPosition = target;
                if (target == LeftSpawnPosition)
                {
                    instance.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                }

                var enemy = instance.GetComponent<EnemyAvatarController>();                
                _enemies.Add(enemy);

                enemy.Activate(true);

                enemy.ConnectedBlockOnGrid = block;
                block.GetComponent<TileAndAvatarConnector>().ConnectAvatar(instance);
            }

            if (_enemies.Count == 1)
            {
                _enemies[0].Select();
            }
        }

        protected void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition + new Vector3(1f, 1f, -1f));
                var enemy = Physics.RaycastAll(ray)
                    .Select(x => x.transform.gameObject)
                    .FirstOrDefault(x => x.CompareTag("Enemy"));

                if (enemy)
                {
                    var avatar = enemy.GetComponent<EnemyAvatarController>();
                    if (avatar.Selected == false)
                    {
                        var current = _enemies.FirstOrDefault(x => x.Selected);
                        if (current)
                        {
                            current.Unselect();
                        }

                        avatar.Select();
                    }                    
                }
            }

            // TODO: Test code
            if (Input.GetKeyUp(KeyCode.Space))
            {
//                CreateEnemy();
            }

            foreach (Transform character in CharacterContainer)
            {
                var combatant = character.GetComponent<ICombatant>();
                if (combatant != null)
                {
                    var strategy = combatant.Strategy();
                    foreach (var action in strategy.Actions)
                    {
                        if (!action.CanExecute())
                        {
                            continue;
                        }

                        if (!ValidatedAction(action))
                        {
                            continue;
                        }

                        action.Execute();

                        if (action.Blocking)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private bool ValidatedAction(CombatantAction action)
        {
            var to = action as MoveTo;
            if (to != null)
            {
                return IsEmptyLocation(to.Target());
            }

            var attack = action as AttackPlayer;
            if (attack != null)
            {
                return IsPlayerLocation(attack.Target());
            }

            var attackEnemy = action as AttackEnemy;
            if (attackEnemy != null)
            {
                return !IsEmptyLocation(attackEnemy.Target());
            }

            return false;
        }

        public bool IsEmptyLocation(Vector3 location)
        {
            return CharacterContainer.Cast<Transform>().All(child => child.localPosition != location);
        }
        public bool IsPlayerLocation(Vector3 location)
        {
            return CharacterContainer.Cast<Transform>().Where(x => x.CompareTag("Player")).Any(child => child.localPosition == location);
        }

        public void AttackEnemyAt(Vector3 location, int damage)
        {
            var enemy = _enemies.FirstOrDefault(x => x.transform.localPosition == location);
            if (enemy)
            {
                DealDamageToEnemy(enemy, damage);
            }
        }

        public void ShootAtSelectedEnemy(int damage)
        {
            var enemy = _enemies.FirstOrDefault(x => x.Selected);
            if (enemy)
            {
                DealDamageToEnemy(enemy, damage);
            }
        }

        private void DealDamageToEnemy(EnemyAvatarController enemy, int damage)
        {
            enemy.Description.CurrentHealth -= damage;
            if (enemy.Description.CurrentHealth > 0)
            {
                Messenger.Instance.Publish(new EnemyUpdatesHealthMessage() { Amount = enemy.Description.CurrentHealth });
            }
            else
            {
                Messenger.Instance.Publish(new DestroyEnemyBlockMessage() { EnemyBlock = enemy.ConnectedBlockOnGrid, });

                DestroyEnemy(enemy);                
            }
        }

        private void DestroyEnemy(EnemyAvatarController enemy)
        {
            _enemies.Remove(enemy);

            enemy.Activate(false);
            enemy.Unselect();
            enemy.Recycle();

            if (_enemies.Count > 0)
            {
                _enemies[0].Select();
            }
            else
            {
                Messenger.Instance.Publish(new LastEnemyDestroyedMessage());
            }
        }

        public int GetDirectionToSelectedEnemy(Vector3 source)
        {
            var enemy = _enemies.FirstOrDefault(x => x.Selected);
            if (enemy == null)
                return 0;

            return enemy.transform.position.x > source.x ? 1 : -1;
        }

        public bool HasSelectedEnemy()
        {
            return _enemies.Any(x => x.Selected);
        }
    }
}
