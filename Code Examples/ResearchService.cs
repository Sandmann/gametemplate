﻿namespace Assets.Scripts.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data.Entities;
    using Data.Models;
    using Infrastructure;
    using SQLite;
    using UnityEngine;

    public class ResearchService : Singleton<ResearchService>
    {
        public readonly string[] PERK_SCAN_SPEED = { "research_perk_scan_1", "research_perk_scan_1" };
        public readonly string[] PERK_COPY_SPEED = { "research_perk_copy_speed_1", "research_perk_copy_speed_2" };        
        public readonly string[] PERK_PROTECTOR_ATTACK = { "research_perk_protector_attack_1", "research_perk_protector_attack_2" };
        public readonly string[] PERK_COOLDOWN_REDUCTION = { "research_perk_cooldown_reduction_1" };
        public readonly string[] PERK_TROJAN_SPEED = { "research_perk_trojan_speed_1" };
        public readonly string[] PERK_TROJAN_STRENGTH = { "research_perk_trojan_strength_1" };
        public readonly string[] PERK_THREAT_INCREASE = { "research_perk_threat_increase_1" };        
        public readonly string[] PERK_DISGUISE = { "research_perk_disguise_1", "research_perk_disguise_2" };
        public readonly string[] PERK_SMART_CODE = { "research_perk_smart_code_1", "research_perk_smart_code_2" };        
        public readonly string[] PERK_VIRUS_STRUCTURE = { "research_perk_virus_structure_1", "research_perk_virus_structure_2", "research_perk_virus_structure_3" };
        public readonly string[] PERK_TROJAN_CAPACITY = { "research_perk_trojan_capacity_1", "research_perk_trojan_capacity_2", "research_perk_trojan_capacity_3" };

        public readonly string[] ABILITY_SPEED = { "research_ability_speed_1", "research_ability_speed_2" };
        public readonly string[] ABILITY_INVISIBLE = { "research_ability_vanish_1", "research_ability_vanish_2" };
        public readonly string[] ABILITY_STUN = { "research_ability_stun_1", "research_ability_stun_2" };
        public readonly string[] ABILITY_BREAK = { "research_ability_break_1", "research_ability_break_2" };

        public readonly string[] TROJAN_RESOURCES_UPGRADE = { "research_trojan_resource_upgrade_1" };
        public readonly string[] TROJAN_IMPROVED_CODE = { "research_trojan_improved_code_1", "research_trojan_improved_code_2" };

        [System.Serializable]
        public class ResearchIconSet
        {
            public ResearchItemType Type;

            // TODO: Move to content controller to load by id of research item
            public Sprite Icon;

            public Sprite UnknownState;
            public Sprite DisabledState;
            public Sprite DisabledStateForeground;
            public Sprite EnabledState;
            public Sprite CompletedState;

            public Sprite LinkEven;
            public Sprite LinkOrdinal;
        }

        public bool UnlockAllTrojans;
        public bool UnlockAllVirusFragments;
        public bool UnlockAllAbilities;

        public float LevelCostFactor = 1.3f;
        public ResearchIconSet[] Icons = new ResearchIconSet[0];

        private ResearchItemEntity[] _items = new ResearchItemEntity[0];
        [HideInInspector]
        public List<ResearchItemModel> Items = new List<ResearchItemModel>();

        private ResearchItemEffectModel[] _effects = new ResearchItemEffectModel[0];

//        public override void Awake()
//        {
//            base.Awake();
//
//            CreateSQLdb();
//        }

        public void Load()
        {
            var db = new SQLiteConnection(Application.dataPath + "/DataBase.db");

            _effects = db.Table<ResearchItemEffectContext>().ToList()
                .Select(x => new ResearchItemEffectModel(x))
                .ToArray();
            _items = db.Table<ResearchItemEntity>().ToArray();

            db.Dispose();

            Items = _items.Select(x => new ResearchItemModel(x, FindEffects(x))).ToList();            
        }

        public static bool EffectDisabled;

        public void LoadFromSave()
        {
            _orderedCompletedResearch = new List<string>();

            if (PersistenceService.CurrentSaveData != null)
            {
                EffectDisabled = true;

                var costByLevel = new List<int>(); // index - level; value - item count
                var completedItems = PersistenceService.CurrentSaveData.ResearchData.Ids;
                for (int i = 0; i < completedItems.Length; i++)
                {
                    SetStateForLinks(completedItems[i], ResearchItemState.Completed);
                    _orderedCompletedResearch.Add(completedItems[i]);

                    var model = Find(completedItems[i]);
                    var dependedModels =
                        Items.Where(x => x.State == ResearchItemState.Disabled && x.Conditions.Contains(model) && x.ConditionsCompleted).ToArray();
                    for (int j = 0; j < dependedModels.Length; i++)
                    {
                        SetStateForLinks(dependedModels[j].Data.Id, ResearchItemState.Enabled);
                    }

                    if (model.Data.Level > costByLevel.Count)
                    {
                        costByLevel.Add(1);
                    }
                    else
                    {
                        costByLevel[model.Data.Level - 1]++;
                    }
                }

                for (int i = 0; i < costByLevel.Count; i++)
                {
                    for (int j = 0; j < costByLevel[i]; j++)
                    {
                        CorrectCostForLevel(i + 1, LevelCostFactor);
                    }
//                    CorrectCostForLevel(i + 1, costByLevel[i] * LevelCostFactor);
                }

                EffectDisabled = false;
            }

            Debug.Log("Research loaded");
        }

        public ResearchDataArray ToData()
        {
            return new ResearchDataArray()
            {
                Ids = _orderedCompletedResearch.ToArray(),
            };
        }

        private List<string> _orderedCompletedResearch = new List<string>();

        public ResearchIconSet GetIconSet(ResearchItemType type)
        {
            return Icons.FirstOrDefault(x => x.Type == type);
        }

        public ResearchItemModel Find(string id)
        {
            return Items.FirstOrDefault(x => x.Data.Id == id);
        }

        public ResearchItemModel[] FindAll(ResearchItemType type, int level)
        {
            return Items.Where(x => x.Type == type && x.Data.Level == level).ToArray();
        }

        public bool CanComplete(string researchId)
        {
            var model = Find(researchId);

            if (model == null)
            {
                return false;
            }

            var cost = model.Cost;
            for (int i = 0; i < cost.Length; i++)
            {
                if (ScrResesController.inst.Reses[i].count < cost[i].count)
                {
                    return false;
                }
            }

            return model.State == ResearchItemState.Enabled && model.ConditionsCompleted;
        }

        public void Complete(string researchId)
        {
            if (!CanComplete(researchId))
            {
                Debug.LogWarning("Try to complete research failed: " + researchId);
                return;
            }

            var model = Find(researchId);
            var cost = model.Cost;
            foreach (var r in cost)
            {
                ScrResesController.inst.TakeRes(r);
            }

            SetStateForLinks(researchId, ResearchItemState.Completed);
            _orderedCompletedResearch.Add(researchId);

            // After research increase others cost with the same level
            CorrectCostForLevel(model.Data.Level, LevelCostFactor);

            // Check not linked but depended models
            var dependedModels =
                Items.Where(x => x.State == ResearchItemState.Disabled && x.Conditions.Contains(model) && x.ConditionsCompleted).ToArray();
            for (int i = 0; i < dependedModels.Length; i++)
            {
                SetStateForLinks(dependedModels[i].Data.Id, ResearchItemState.Enabled);
            }

            if (model.Type == ResearchItemType.PlayerAbility)
            {
                AbilityController.Instance.UpdateAbilities();
            }
            if (model.Type == ResearchItemType.Trojan && model.Effects.Any(x => !string.IsNullOrEmpty(x.Target)))
            {
                ScrTrojanController.inst.UpdateTrojans();
            }

            AchievementService.Instance.CompleteResearch();
        }

        private void CorrectCostForLevel(int level, float costFactor)
        {
            var levelModels = Items.Where(x => x.State != ResearchItemState.Completed && x.Data.Level == level).ToArray();
            for (int i = 0; i < levelModels.Length; i++)
            {
                foreach (var r in levelModels[i].Cost)
                {
                    r.count = (int)(r.count * costFactor);
                }
            }
        }

        public void SetStateForLinks(string researchId, ResearchItemState state)
        {
            var model = Find(researchId);
            if (model != null)
            {
                model.State = state;                

                for (int i = 0; i < model.Links.Count; i++)
                {
                    var link = model.Links[i];
                    if (model.State == ResearchItemState.Completed && (link.State == ResearchItemState.Unknown || link.State == ResearchItemState.Disabled) &&
                        link.ConditionsCompleted)
                    {
                        SetStateForLinks(link.Data.Id, ResearchItemState.Enabled);
                    }
                    else if (model.State == ResearchItemState.Enabled && (link.State == ResearchItemState.Unknown))
                    {
                        SetStateForLinks(link.Data.Id, ResearchItemState.Disabled);
                    }
                }
            }
        }

        public bool IsVirusFragmentOpened(string id)
        {
            return UnlockAllVirusFragments || Items.Any(x =>
                x.Type == ResearchItemType.Virus && (x.State == ResearchItemState.Completed || x.Data.Opened) &&
                x.Effects.Any(e => e.Target == id));
        }

        public bool IsPlayerAbilityOpened(string id)
        {
            return UnlockAllAbilities || Items.Any(x =>
                x.Type == ResearchItemType.PlayerAbility && (x.State == ResearchItemState.Completed || x.Data.Opened) &&
                x.Effects.Any(e => e.Target == id));
        }

        public bool IsTrojanOpened(string id)
        {
            return UnlockAllTrojans || Items.Any(x =>
                x.Type == ResearchItemType.Trojan && (x.State == ResearchItemState.Completed || x.Data.Opened) &&
                x.Effects.Any(e => e.Target == id));
        }

        public int GetAbilityBonus(string ability)
        {
            var codes = new[] { "" };
            if (ability == AbilityModel.SPEED_ABILITY)
            {
                codes = ABILITY_SPEED;
            }
            if (ability == AbilityModel.INVISIBLE_ABILITY)
            {
                codes = ABILITY_INVISIBLE;
            }
            if (ability == AbilityModel.STUN_ABILITY)
            {
                codes = ABILITY_STUN;
            }
//            if (ability == AbilityModel.BREAK_ABILITY)
//            {
//                codes = ABILITY_BREAK;
//            }

            var items = Items.Where(x => x.State == ResearchItemState.Completed && codes.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

            if (items.Length > 0)
            {
                return items.Max(x => x.Value);
            }

            return 0;
        }

        public int BreakAbilityBonus
        {
            get
            {
                var items = Items.Where(x => x.State == ResearchItemState.Completed && ABILITY_BREAK.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int TrojanResourcesBonus
        {
            get
            {
                var items = Items.Where(x => x.State == ResearchItemState.Completed && TROJAN_RESOURCES_UPGRADE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int TrojanLevelBonus
        {
            get
            {
                var items = Items.Where(x => x.State == ResearchItemState.Completed && TROJAN_IMPROVED_CODE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int ScanSpeed
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_SCAN_SPEED.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int CopySpeed
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_COPY_SPEED.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int ProtectorAttack
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_PROTECTOR_ATTACK.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int AbilityCooldownReduction
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_COOLDOWN_REDUCTION.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int TrojanSpeed
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_TROJAN_SPEED.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int TrojanStrength
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_TROJAN_STRENGTH.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int ThreatIncrease
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_THREAT_INCREASE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int Disguise
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_DISGUISE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int SmartCode
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_SMART_CODE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int VirusExtraSlotAmount
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_VIRUS_STRUCTURE.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        public int TrojanCapacity
        {
            get
            {
                var items = Items.Where(x => (x.State == ResearchItemState.Completed || x.Data.Opened) && PERK_TROJAN_CAPACITY.Any(y => y == x.Data.Id))
                    .SelectMany(x => x.Effects)
                    .ToArray();

                if (items.Length > 0)
                {
                    return items.Max(x => x.Value);
                }

                return 0;
            }
        }

        private void CreateSQLdb()
        {
            var db = new SQLiteConnection(Application.dataPath + "/DataBase.db");
        
//            db.CreateTable<ResearchItemEntity>();
            db.CreateTable<ResearchItemEffectContext>();
        
            db.Dispose();
        }

        private readonly string[] EFFECT_SEPARATOR = { ";" };
        private ResearchItemEffectModel[] FindEffects(ResearchItemEntity context)
        {
            if (string.IsNullOrEmpty(context.Effects))
            {
                return new ResearchItemEffectModel[0];
            }

            var effects = context.Effects.Split(EFFECT_SEPARATOR, StringSplitOptions.RemoveEmptyEntries);
            return effects.Select(x => _effects.FirstOrDefault(e => e.ContextId == x)).ToArray();
        }
    }
}
