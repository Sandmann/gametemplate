﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Spine.Unity;
using Spine.Unity.Modules;
using UnityEngine;
using UnityEngine.Profiling;

public class WorkerView : MonoBehaviour
{
    public SpriteRenderer Background;
    public SkeletonAnimation SkeletonAnimation;

    public SpriteRenderer Border;
    public GameObject BorderOutline;

    public Sprite BorderOn;
    public Sprite BorderOff;

    public Sprite[] Borders;
    private Sprite _borderOn, _borderOff;

    public bool IsNpc;

    [HideInInspector] public Camera WorldCamera;
    [HideInInspector] public CardController CardController;

    private BoxCollider _boxCollider;
    private CellView _cellView;

    private readonly string[] _embassadorSkins = new[] { "Skirt1", "Skirt2", "Skirt3", "Skirt4" };
    private int _colorProperty;

    private SpriteRenderer _borderOutlineRender;
    private MeshRenderer _animationRender;

    private SkeletonRenderer _skeletonRenderer;
    public SkeletonRenderer SkeletonRenderer
    {
        get
        {
            return _skeletonRenderer ?? (_skeletonRenderer = SkeletonAnimation.GetComponent<SkeletonRenderer>());
        }
    }

    public float Width { get { return Border.sprite.bounds.size.x; } }    

    protected void Awake()
    {
        if (!IsNpc)
        {
            _borderOff = Borders[0];
            _borderOn = Borders[1];
            Border.sprite = _borderOff;            
        }        
    }

    protected void Start()
    {
        _colorProperty = Shader.PropertyToID("_Color");
        
        _boxCollider = GetComponent<BoxCollider>();
        _cellView = GetComponentInParent<CellView>();
    }

    protected void OnMouseDown()
    {
        if (MouseLocker.MouseLocked || IsNpc)
            return;

        GameContext.Current.SelectedCell = _cellView.GetContext();

        CardController.selectedTime = Time.time;
        CardController.OnStartMoving(_cellView);
    }

    private void OnMouseEnter()
    {
        if (IsNpc)
        {
            return;
        }

        var locked = EffectQueue.IsRunning || CardController.InDrag;
        Border.sprite = locked
            ? _borderOff
            : _borderOn;
    }

    private void OnMouseExit()
    {
        if (IsNpc)
        {
            return;
        }
        
        Border.sprite = _borderOff;
    }

    internal void SetBackground(string workerBackground)
    {
        Background.sprite = WorkerBackgroundProvider.GetSprite(workerBackground);
    }
    
    public void SetBorderOutline(bool isEnabled)
    {
        BorderOutline.SetActive(isEnabled);
    }

    private Coroutine _animationDelay;
    public void SetAnimation(string animationId, string head, System.Action bgSwitch = null)
    {
        if (!gameObject.activeInHierarchy)
        {
            if (bgSwitch != null)
            {
                bgSwitch();
            }
            SetAnimationInner(animationId, head);
            return;
        }

        if (_animationDelay != null)
        {
            StopCoroutine(_animationDelay);
        }

        _animationDelay = StartCoroutine(SetAnimationDelay(animationId, head, bgSwitch));
    }

    private IEnumerator SetAnimationDelay(string animationId, string head, System.Action bgSwitch)
    {
        yield return new WaitForSeconds(0.01f);
        yield return null;

        SetAnimationInner(animationId, head);
        if (bgSwitch !=null)
        {
            bgSwitch();
        }
    }

    private void SetAnimationInner(string animationId, string head)
    {
        Profiler.BeginSample("Worker view. Set animation. Deactivate");
        SkeletonAnimation.gameObject.SetActive(false);
        Profiler.EndSample();

        Profiler.BeginSample("Worker view. Set animation. Load data");
        var skeleton = WorkerAnimationController.GetSkeletonData(animationId);
        Profiler.EndSample();

        if (skeleton == null)
        {
            Debug.LogErrorFormat("Missing animation with id: '{0}'", animationId);
            return;
        }        

        if (SkeletonAnimation.SkeletonDataAsset != skeleton)
        {
            SkeletonAnimation.skeletonDataAsset = skeleton;

            Profiler.BeginSample("Worker view. Set animation. Set animation name");
            var defaultAnim = skeleton.GetSkeletonData(true).Animations.First();
            SkeletonAnimation.AnimationName = defaultAnim.Name;
            Profiler.EndSample();

            Profiler.BeginSample("Worker view. Set animation. Clear data");
            foreach (AtlasAsset aa in SkeletonAnimation.skeletonDataAsset.atlasAssets)
            {
                if (aa != null)
                {
                    aa.Clear();
                }                    
            }
            SkeletonAnimation.skeletonDataAsset.Clear();
            Profiler.EndSample();

            Profiler.BeginSample("Worker view. Set animation. Initialize");
            SkeletonAnimation.Initialize(true);
            Profiler.EndSample();

            // Set random skirt for embassador anim (it uses skins)
            if (animationId == "work_man_embassador")
            {
                SkeletonAnimation.Skeleton.SetSkin(_embassadorSkins.RandomElement());
            }

            Profiler.BeginSample("Worker view. Set animation. Replace head");
            if (WorkerAnimationController.CanReplaceHead(animationId) && !string.IsNullOrEmpty(head))
            {
                var attachement = SkeletonRenderer.skeleton.AttachUnitySprite("Head", WorkerHeadProvider.GetIcon(head), "Custom/SpineStencil");
                var material = attachement.GetMaterial();                
                if (material.HasProperty("_Stencil"))
                {
                    var value = IsNpc ? 2 : 4;
                    material.SetFloat("_Stencil", value);
                }                
            }
            Profiler.EndSample();

            // Replace for NPC animation stencil buffer to avoid an overlap with Worker animation
            if (IsNpc)
            {
                Profiler.BeginSample("Worker view. Set animation. Override material");
                var atlas = SkeletonAnimation.SkeletonDataAsset.atlasAssets.First();
                var originalMaterial = atlas.materials[0];

                if (!SkeletonAnimation.CustomMaterialOverride.ContainsKey(originalMaterial))
                {
                    var overrideMaterial = new Material(originalMaterial);
                    if (overrideMaterial.HasProperty("_Stencil"))
                    {
                        overrideMaterial.SetFloat("_Stencil", 2);
                    }

                    SkeletonAnimation.CustomMaterialOverride.Add(originalMaterial, overrideMaterial);
                }
                Profiler.EndSample();
            }
        }

        // Restore alpha for animation material
        Profiler.BeginSample("Worker view. Set animation. Restore alpha");
        var materials = SkeletonAnimation.CustomMaterialOverride.Values.Where(x => x.HasProperty(_colorProperty));
        foreach (var material in materials)
        {
            var color = material.GetColor(_colorProperty);
            color.a = 1f;
            material.SetColor(_colorProperty, color);
        }
        Profiler.EndSample();

        SkeletonAnimation.timeScale = Global.AnimationDisabled ? 0f : 1f;

        Profiler.BeginSample("Worker view. Set animation. Activate");
        SkeletonAnimation.gameObject.SetActive(true);
        Profiler.EndSample();
    }
}
