﻿using UnityEngine;

namespace Assets.Scripts.Infrastructure
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (_instance == null)
                    {
                        var obj = new GameObject(typeof(T).Name);
                        obj.hideFlags = HideFlags.DontSave;

                        _instance = obj.AddComponent<T>();
                    }
                }

                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;

                // XXX: To keep children of singleton... we need to use transform.
                DontDestroyOnLoad(this.transform.gameObject);
            }
            else if (_instance != this as T)
            {
                DestroyImmediate(gameObject);

                return;
            }            
        }
    }
}
