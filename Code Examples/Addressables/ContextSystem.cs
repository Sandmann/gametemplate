﻿namespace Assets.Game.Core.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using UnityEngine;

    public abstract class GameSystem
    {
        private static Dictionary<string, GameSystem> _systems = new Dictionary<string, GameSystem>();

        public static T Create<T>() where T : GameSystem, new()
        {
            var sys = new T();
            sys.Initialize();

            Add(sys);
            return sys;
        }

        public static void Add(GameSystem sys)
        {
            _systems[sys.GetType().FullName] = sys;
        }

        public static T Get<T>() where T : GameSystem
        {
            var typeName = typeof(T).FullName;
            if (typeName == null || !_systems.ContainsKey(typeName))
            {
                Debug.LogWarning($"System is missing: {typeName}");
                return null;
            }

            return _systems[typeName] as T;
        }

        public virtual void Initialize()
        {
        }

        public virtual async Task Update()
        {
            await Task.Yield();
        }

        public virtual async Task Setup()
        {
            await Task.Yield();
        }
    }

    public interface IMenuSystem
    {
        void ShowMenu();
        void HideMenu();
    }
}
