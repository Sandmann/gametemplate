﻿namespace Assets.Game.Domain.Systems
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Eventing.Reader;
    using System.Linq;
    using System.Threading.Tasks;
    using Contexts;
    using Core.Common;
    using Extensions;
    using Models;
    using UnityEngine;

    [Serializable]
    public class ActivitySystem : GameSystem
    {
        public CardContext GetActivity(uint activityId)
        {
            return GameState.Current.Cards.FirstOrDefault(x => x.Id == activityId && x.Type == CardType.Activity);
        }
        public CardContext GetActivityForWorker(uint workerId)
        {
            return GameState.Current.Cards.FirstOrDefault(x => x.Owner == workerId && x.Type == CardType.Activity);
        }

        public CardContext GetPermanent(uint activityId)
        {
            return GameState.Current.Cards.FirstOrDefault(x => x.Owner == activityId && x.Type == CardType.Permanent);
        }

        public override async Task Update()
        {
            await base.Update();

            var stats = Get<StatSystem>();
            var workers = Get<WorkerSystem>();

            var activities = GameState.Current.Cards
                .Where(x => x.Type == CardType.Activity && x.Duration > 0)
                .ToArray();
            for (int i = 0; i < activities.Length; i++)
            {
                if (activities[i].Model.Id == "card_build")
                {
                    if (stats.Production.CurValue < 3f)
                    {
                        continue;
                    }

                    Get<StatSystem>().AddProduction(-3f);
                }
                if (activities[i].Model.Id == "card_clean_landscape")
                {
                    if (stats.Production.CurValue < 5f)
                    {
                        continue;
                    }

                    Get<StatSystem>().AddProduction(-5f);
                }
                if (activities[i].Model.Id == "card_hunting")
                {
                    if (stats.Production.CurValue < 1f)
                    {
                        continue;
                    }

                    Get<StatSystem>().AddProduction(-1f);
                    Get<StatSystem>().AddFood(+1f);
                }

                activities[i].Duration--;
                activities[i].UpdateViews();

                if (activities[i].Duration == 0)
                {
                    await CompleteActivity(activities[i]);
                }
            }
        }

        public async Task CreateActivity(CardContext decision, WorkerContext worker, CardContext permanent)
        {
            // играем действия против событий, отдельная ветка без активити
            if (permanent.Type == CardType.GameEvent)
            {
                Get<StatSystem>().PayCost(decision.Cost);

                if (decision.Model.Id == "card_defense" ||
                    decision.Model.Id == "card_bribe")
                {
                    var reward = Mathf.RoundToInt(Get<DecisionSystem>().GetReward(decision));
                    await Get<GameEventSystem>().AddEnemyAttackLevel(permanent, -reward);
                }

                if (decision.Model.Id == "card_attack")
                {
                    var enemyArmy = GameState.Current.GameContext.EventParams
                        .FirstOrDefault(x => x.Id == permanent.Id)?.Value ?? 0;
                    var battle = Get<BattleSystem>().EvaluateBattle(new Combatant()
                    {
                        Army = Mathf.RoundToInt(Get<StatSystem>().Army.CurValue),
                        IsPlayer = true,
                    }, new Combatant()
                    {
                        Army = enemyArmy,
                    });

                    // наносим потери
                    Get<StatSystem>().AddArmy(-battle.AttackerLoss);
                    await Get<GameEventSystem>().AddEnemyAttackLevel(permanent, -battle.DefenderLoss);
                }

                return;
            }

            // Отменить текущую активити
            var activity = GetActivityForWorker(worker.Id);
            if (activity != null)
            {
                if (activity.Model.Id == decision.Model.Id)
                {
                    Debug.Log($"Worker already has that activity {decision.Model.Id}");
                    return;
                }

                CancelActivity(worker);
            }

            // Создать новую
            activity = GameState.Current.CreateContext<CardContext, Card>(decision.Model);
            activity.Owner = worker.Id;
            activity.Type = CardType.Activity;
            activity.Tags = new List<CardTag>(activity.Model.Tags);
            
            // TODO: костыль, для расчета наград при завершении активити
            activity.Cost = decision.Cost;

            permanent.Owner = activity.Id;

            if (decision.Model.Id == "card_household")
            {
                // Обновляем модификаторы статов от активити
                Get<ModifiersSystem>().AddModifierToStat(Stats.Food, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Food, worker.Location), activity.Id);

                Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Production, worker.Location), activity.Id);

                Get<ModifiersSystem>().AddModifierToStat(Stats.Culture, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Culture, worker.Location), activity.Id);

                Get<ModifiersSystem>().AddModifierToStat(Stats.Army, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Army, worker.Location), activity.Id);

                Get<ModifiersSystem>().AddModifierToStat(Stats.Gold, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Gold, worker.Location), activity.Id);

                Get<ModifiersSystem>().AddModifierToStat(Stats.Faith, ModifierGroup.FromLocations, 
                    GetStatModifierByPermanent(Stats.Faith, worker.Location), activity.Id);
            }

            var effects = activity.Model.Triggers
                .Where(x => x.State == CardTriggerState.OnPlay)
                .SelectMany(x => x.Effects)
                .ToArray();
            for (int i = 0; i < effects.Length; i++)
            {
                await Get<CardEffectSystem>().ResolveEffect(effects[i], activity, permanent);
            }

            Debug.Log($"Create: {activity.Model.Id}");

            if (!activity.Is(CardTag.Work))
            {
                Get<StatSystem>().PayCost(decision.Cost);

                if (activity.Duration == 0)
                {
                    await CompleteActivity(activity);
                }
                else
                {
                    if (activity.Model.Id == "card_build")
                    {
                        Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromActivities, -3f, activity.Id);
                    }
                    if (activity.Model.Id == "card_clean_landscape")
                    {
                        Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromActivities, -5f, activity.Id);
                    }
                    if (activity.Model.Id == "card_hunting")
                    {
                        Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromActivities, -1f, activity.Id);
                        Get<ModifiersSystem>().AddModifierToStat(Stats.Food, ModifierGroup.FromActivities, 1f, activity.Id);
                    }
                }
            }
        }

        public async Task CompleteActivity(CardContext activity)
        {
            Debug.Log($"Complete: {activity.Model.Id}");

            var permanent = GetPermanent(activity.Id);
            var worker = Get<WorkerSystem>().GetWorker(activity.Owner);
            var location = Get<MapSystem>().GetLocation(worker.Location);

            // пробуем разогнать зверей по соседним локациям
            // делаем это до возврата переманента из активити
            // (чтобы при обновлении локации у активити была цель не null)
            if (activity.Is(CardTag.Building))
            {
                //TODO: отключаем по дизайну бегство зверей от построек
//                var animals = Get<MapSystem>().GetPermanents(worker.Location)
//                    .Where(x => x.Is(CardTag.Animal, CardTag.Wild) && !x.Is(CardTag.Unexplored))
//                    .ToArray();
//                for (int i = 0; i < animals.Length; i++)
//                {
//                    await Get<AnimalSystem>().TryMoveAnimalToRandomLocation(animals[i]);
//                }


//                if (activity.Model.Id == "card_build_field")
//                {
//                    // содержание зданий стоит -1 продукции
//                    Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromBuildings, -1f, GameState.Current.GameContext.Id);
//                }
//
//                // пробуем уничтожить жилье
//                if (activity.Model.Id == "card_build_workshop" ||
//                    activity.Model.Id == "card_build_gymnasia" ||
//                    activity.Model.Id == "card_build_shrine" ||
//                    activity.Model.Id == "card_build_barracks")
//                {
//                    var houses = Get<MapSystem>()
//                        .GetPermanents(location.Id)
//                        .FirstOrDefault(x => x.Model.Id == "card_houses");
//                    if (houses != null)
//                    {
//                        Get<MapSystem>().DestroyPermanent(houses);
//                    }
//                }
            }

            if (activity.Model.Id == "card_build")
            {
                var buildings = GameState.Current.Map.Model.Buildings
                    .FirstOrDefault(x => x.Owner == location.Model.Id);
                if (buildings != null)
                {
                    var curBuilding = Get<MapSystem>().GetPermanents(location.Id).Count(x => x.Is(CardTag.Building));
                    if (curBuilding < buildings.Cards.Length)
                    {
                        await Get<MapSystem>().CreatePermanent(buildings.Cards[curBuilding].Card, location.Id);
                    }
                }
            }

            // Вернуть перманент из активити обратно в локацию
            permanent.Owner = worker.Location;

            // применяем эффекты активити
            var isTaming = activity.Model.Id == "card_expansion" && permanent.Is(CardTag.Animal);
            if (activity.Model.Id == "card_taming" || isTaming)
            {
                permanent.Tags.Add(CardTag.Pet);

                permanent.Tags.Remove(CardTag.Wild);
                permanent.Tags.Remove(CardTag.Tamable);
                permanent.Tags.Remove(CardTag.Aggressive);

//                var prop = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_defense");
//                if (prop != null)
//                {
//                    GameState.Current.Cards.Remove(prop);
//                }
            }
            var isLooting = activity.Model.Id == "card_expansion" && permanent.Is(CardTag.Loot);
            if (isLooting)
            {
                if (permanent.Model.Id == "card_loot_food")
                {
                    Get<StatSystem>().AddFood(Get<DecisionSystem>().RewardForLoot);
                }
                if (permanent.Model.Id == "card_loot_army")
                {
                    Get<StatSystem>().AddArmy(Get<DecisionSystem>().RewardForLoot);
                }
                if (permanent.Model.Id == "card_loot_culture")
                {
                    Get<StatSystem>().AddCulture(Get<DecisionSystem>().RewardForLoot);
                }
                if (permanent.Model.Id == "card_loot_gold")
                {
                    Get<StatSystem>().AddGold(Get<DecisionSystem>().RewardForLoot);
                }
                if (permanent.Model.Id == "card_loot_production")
                {
                    Get<StatSystem>().AddProduction(Get<DecisionSystem>().RewardForLoot);
                }

                Get<MapSystem>().DestroyPermanent(permanent);
            }

            if (activity.Model.Id == "card_clean_landscape" && permanent.Model.Id == "card_forest")
            {
                var reward = Get<DecisionSystem>().GetReward(activity);
                if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
                {
                    reward *= 2;
                }

                Get<StatSystem>().AddProduction(reward);
                Get<MapSystem>().DestroyPermanent(permanent);
            }
            if (activity.Model.Id == "card_clean_landscape" && permanent.Model.Id == "card_swamp")
            {
                var reward = GameState.Current.GameContext
                    .PermanentParams.FirstOrDefault(x => x.Id == permanent.Id)?.Value ?? 0;
                if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
                {
                    reward *= 2;
                }

                Get<StatSystem>().AddFood(reward);
                Get<MapSystem>().DestroyPermanent(permanent);
            }
//            if (activity.Model.Id == "card_attack" && permanent.Model.Id == "card_ruins")
//            {
//                Get<StatSystem>().AddCulture(3f);
//            }
            if (activity.Model.Id == "card_clean_landscape" && permanent.Model.Id == "card_cursed_place")
            {
                var reward = GameState.Current.GameContext.RewardForCursed;
                if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
                {
                    reward *= 2;
                }

                Get<StatSystem>().AddFaith(reward);
                GameState.Current.GameContext.CleanedCursed += 1;

                Get<ModifiersSystem>().RemoveAllModifiers(permanent.Id);
                Get<MapSystem>().DestroyPermanent(permanent);
            }

            if ((activity.Model.Id == "card_attack") && permanent.Is(CardTag.Tribe))
            {
                var enemyArmy = Get<MapSystem>().GetProperties(permanent.Id)
                    .FirstOrDefault(x => x.Is(CardTag.EnemyArmy));
                if (enemyArmy == null || enemyArmy.Duration == 0)
                {
                    // Если у племени нет армии, уничтожаем его
                    Get<MapSystem>().DestroyPermanent(permanent);
                }
                else
                {
                    var battle = Get<BattleSystem>().EvaluateBattle(new Combatant()
                    {
                        Army = Mathf.RoundToInt(Get<StatSystem>().Army.CurValue),
                        IsPlayer = true,
                    }, new Combatant()
                    {
                        Army = activity.Model.Id == "card_raid"
                            ? enemyArmy.Duration / 2 // рейд игрока на 50% армии племени
                            : enemyArmy.Duration
                    });

                    if (battle.AttackerWin)
                    {
                        // игрок побеждает, наносим потери; если армия племени уничтожена, удаляем ее
                        Get<StatSystem>().AddArmy(-battle.AttackerLoss);
                        
                        // Наносим потери племени
                        enemyArmy.Duration -= battle.DefenderLoss;
                        if (enemyArmy.Duration <= 0)
                        {
                            Get<DiplomacySystem>().EndAllDeals(permanent);
                            Get<MapSystem>().DestroyPermanent(permanent);
                        }
                    }
                    else
                    {
                        // игрок проигрывает
                        Get<StatSystem>().AddArmy(-battle.AttackerLoss);
                        enemyArmy.Duration -= battle.DefenderLoss;
                    }

                    // портим отношения
                    var relations = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_relations");
                    if (relations != null)
                    {
                        relations.Duration = Mathf.Max(0, relations.Duration - 20);
                        relations.UpdateViews();
                    }
                }
            }

            if (activity.Model.Id == "card_raid")
            {
                var stat = Get<StatSystem>().FindContext(Stats.CodeToStat((StatCode) permanent.GetAttributeValue(CardAttributeType.Export)));
                if (stat != null)
                {
                    // грабим ресурс экспорта племени
                    var reward = Get<DecisionSystem>().GetReward(activity);
                    if (Get<TechSystem>().IsTechResearched("card_unlock_raid"))
                    {
                        reward += 3;
                    }

                    Get<StatSystem>().AddValueToStat(stat.Model.Id, reward);
                }

                // портим отношения
                var relations = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_relations");
                relations.Duration = Mathf.Max(0, relations.Duration - 1);
            }

//            if (activity.Model.Id == "card_attack" && permanent.Is(CardTag.Tribe))
//            {
//                var defense = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_defense");
//                if (defense != null && defense.Duration > 0)
//                {
//                    defense.Duration--;
//                    defense.UpdateViews();
//
//                    // портим отношения
//                    var relations = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_relations");
//                    relations.Duration = Mathf.Max(0, relations.Duration - 1);
//                }
//                else
//                {
//                    Get<MapSystem>().DestroyPermanent(permanent);
//                }
//            }

            if (activity.Model.Id == "card_exploring")
            {
                if (permanent.Model.Id == "card_ruins")
                {
                    // Уничтожаем руины
                    Get<MapSystem>().DestroyPermanent(permanent);

                    string unique = null;
                    if (Get<MapSystem>().HasPermanent(location.Id, "card_hills"))
                    {
                        if (Get<MapSystem>().HasPermanent(location.Id, "card_houses"))
                        {
                            // в локе есть дома, уничтожаем их и заменяем уникальной постройкой
                            var houses = Get<MapSystem>()
                                .GetPermanents(location.Id)
                                .FirstOrDefault(x => x.Model.Id == "card_houses");
                            Get<MapSystem>().DestroyPermanent(houses);

                            unique = (new List<string>()
                            {
                                "card_workshop",
                                "card_gymnasia",
                                "card_academy",
                                "card_factory",
                            }).GetRandom();
                        }
                        else if (!Get<MapSystem>().HasPermanentAny(location.Id, CardTag.Building))
                        {
                            // в локе ничего не построено, выбираем любую постройку
                            unique = (new List<string>()
                            {
                                "card_houses",
                                "card_workshop",
                                "card_gymnasia",
                                "card_academy",
                                "card_factory",
                            }).GetRandom();
                        }
                    }
                    else
                    {
                        if (Get<MapSystem>().HasPermanent(location.Id, "card_field"))
                        {
                            // в локе есть поля, уничтожаем их и заменяем уникальной постройкой
                            var field = Get<MapSystem>()
                                .GetPermanents(location.Id)
                                .FirstOrDefault(x => x.Model.Id == "card_field");
                            Get<MapSystem>().DestroyPermanent(field);

                            unique = "card_irrigation";
                        }
                        else if (!Get<MapSystem>().HasPermanentAny(location.Id, CardTag.Building))
                        {
                            // в локе ничего не построено, выбираем любую постройку
                            unique = (new List<string>()
                            {
                                "card_field",
                                "card_irrigation",
                            }).GetRandom();
                        }
                    }

                    if (unique != null)
                    {
                        var ctx = await Get<MapSystem>().CreatePermanent(unique, location.Id);
                        ctx.Tags.Add(CardTag.Ruined);

                        if (unique == "card_academy" ||
                            unique == "card_field" ||
                            unique == "card_irrigation")
                        {
                            Get<ModifiersSystem>().AddModifierToStat(Stats.Production, ModifierGroup.FromBuildings, -1f, GameState.Current.GameContext.Id);
                        }
                    }
                }
                else
                {
                    if (permanent.Is(CardTag.Landscape))
                    {
                        Get<MapSystem>().ExploreLocation(worker.Location);
                    }
                    else
                    {
                        permanent.Tags.Remove(CardTag.Unexplored);
                        permanent.UpdateViews();
                    }
                }
            }

            if (activity.Model.Id == "card_expansion" && !isTaming && !isLooting)
            {
                Get<MapSystem>().GainControlOfLocation(location);

                // уничтожаем племя
                var tribe = Get<MapSystem>().GetPermanents(location.Id).FirstOrDefault(x => x.Is(CardTag.Tribe));
                if (tribe != null)
                {
                    Get<DiplomacySystem>().EndAllDeals(permanent);

                    // часть армии племени переходит игроку
                    if (Get<DiplomacySystem>().GetRelations(tribe.Id) == 100)
                    {
                        var army = Get<DiplomacySystem>().GetTribeArmy(tribe.Id);
                        Get<StatSystem>().AddArmy(0.2f * army);
                    }

                    Get<MapSystem>().DestroyPermanent(tribe);
                }
            }

            if (activity.Model.Id == "card_repair")
            {
                permanent.Tags.Remove(CardTag.Ruined);
                permanent.UpdateViews();
            }

            if (activity.Model.Id == "card_alarm")
            {
                var card = GameState.Current.Cards
                    .FirstOrDefault(x => x.Type == CardType.Decision && x.Model.Id == "card_alarm");
                if (card != null)
                {
                    var reward = Get<DecisionSystem>().GetReward(activity);
                    Get<StatSystem>().AddArmy(reward);
                }
            }

//            if (activity.Is(CardTag.Diplomacy))
//            {
//                await Get<DiplomacySystem>().RunDeal(activity.Model.Id, permanent);
//            }
            if (activity.Model.Id == "card_trade_buy")
            {
                if (Get<MapSystem>().IsTrader(permanent.Model.Id))
                {
                    var amount = GetStatToTrade(permanent.Model.Id, 0, activity.Model.Id);
                    if (Get<TechSystem>().IsTechResearched("card_unlock_trade"))
                    {
                        amount *= 1.2f;
                    }

                    if (permanent.Model.Id == "card_egypt_traders")
                    {
                        Get<StatSystem>().AddFood(amount);
                    }
                    if (permanent.Model.Id == "card_carthago_traders")
                    {
                        Get<StatSystem>().AddArmy(amount);
                    }
                    if (permanent.Model.Id == "card_greece_traders")
                    {
                        Get<StatSystem>().AddCulture(amount);
                    }
                    if (permanent.Model.Id == "card_syria_traders")
                    {
                        Get<StatSystem>().AddProduction(amount);
                    }
                }
                else
                {
                    var reward = Get<DecisionSystem>().GetReward(activity);
                    if (Get<TechSystem>().IsTechResearched("card_unlock_trade"))
                    {
                        reward *= 1.2f;
                    }

                    var stat = Stats.CodeToStat((StatCode) permanent.GetAttributeValue(CardAttributeType.Export));
                    Get<StatSystem>().AddValueToStat(stat, reward);
                }
            }

            if (activity.Model.Id == "card_talks")
            {
                var reward = Mathf.RoundToInt(Get<DecisionSystem>().GetReward(activity));
                var relations = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_relations");
                relations.Duration = Mathf.Min(100, relations.Duration + reward);
                relations.UpdateViews();
            }

            if (activity.Model.Id == "card_culture_exchange")
            {
                var reward = Get<DecisionSystem>().GetReward(activity);
                if (Get<TechSystem>().IsTechResearched("card_improve_culture_exchange"))
                {
                    reward *= 1.2f;
                }
                Get<StatSystem>().AddCulture(reward);

                var relations = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_relations");
                relations.Duration = Mathf.Min(100, relations.Duration + Mathf.RoundToInt(reward));
                relations.UpdateViews();
            }

            if (activity.Model.Id == "card_union")
            {
                var reward = Get<DecisionSystem>().GetReward(activity);
                if (Get<TechSystem>().IsTechResearched("card_improve_union"))
                {
                    reward *= 1.2f;
                }
                Get<StatSystem>().AddArmy(reward);
            }

            if (activity.Model.Id == "card_bribe")
            {
                var reward = Mathf.RoundToInt(Get<DecisionSystem>().GetReward(activity));
                
                var army = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_army");
                army.Duration = Mathf.Max(0, army.Duration - reward);
                army.UpdateViews();
            }

//            if (activity.Model.Id == "card_trade_foreign")
//            {
//                var battle = Get<BattleSystem>().EvaluateBattle(new Combatant()
//                {
//                    Army = Mathf.RoundToInt(Get<StatSystem>().Army.CurValue),
//                    IsPlayer = true,
//                }, new Combatant()
//                {
//                    Army = permanent.Duration,
//                });
//
//                Get<StatSystem>().AddArmy(-battle.AttackerLoss);
//                permanent.Duration -= battle.DefenderLoss;
//
//                if (battle.AttackerWin)
//                {
//                    var amount = GetStatToTrade(permanent.Model.Id, 0, activity.Model.Id);
//                    if (permanent.Model.Id == "card_egypt_traders")
//                    {
//                        Get<StatSystem>().AddFood(amount);
//                    }
//                    if (permanent.Model.Id == "card_carthago_traders")
//                    {
//                        Get<StatSystem>().AddArmy(amount);
//                    }
//                    if (permanent.Model.Id == "card_greece_traders")
//                    {
//                        Get<StatSystem>().AddCulture(amount);
//                    }
//                    if (permanent.Model.Id == "card_syria_traders")
//                    {
//                        Get<StatSystem>().AddProduction(amount);
//                    }

//                    if (permanent.Duration <= 0)
//                    {
//                        Get<MapSystem>().DestroyPermanent(permanent);
//                    }
//                }

                // рейд на торговцев отменяет их событие, эти торговцы больше не приплывают к игроку
//                if (activity.Model.Id == "card_raid")
//                {
//                    GameState.Current.GameContext.ExcludeTraders.Add(permanent.Model.Id);
//                    await Get<GameEventSystem>().ForceEndGlobalEvent("card_event_foreign_trade");
//                }
//            }


            if (activity.Model.Id == "card_build_temple_zeus")
            {
                var temple = Get<MapSystem>().GetPermanents(location.Id)
                    .FirstOrDefault(x => x.Model.Id == "card_temple_zeus");
                if (temple == null)
                {
                    temple = await Get<MapSystem>().CreatePermanent("card_temple_zeus", location.Id);
                }

                temple.Duration += 1;
            }

            if (activity.Model.Id == "card_clean")
            {
                if (permanent.Is(CardTag.Diplomacy))
                {
                    Get<DiplomacySystem>().EndDeal(permanent);
                }
                else
                {
                    Get<MapSystem>().DestroyPermanent(permanent);
                }
            }

            if (activity.Model.Id == "card_excuse")
            {
                Get<MapSystem>().DestroyPermanent(permanent);
            }

            // откатываем модификаторы активити
            Get<ModifiersSystem>().RemoveAllModifiers(activity.Id);

            // применяем эффекты активити
            var effects = activity.Model.Triggers
                .Where(x => x.State == CardTriggerState.OnFinish)
                .SelectMany(x => x.Effects)
                .ToArray();
            for (int i = 0; i < effects.Length; i++)
            {
                await Get<CardEffectSystem>().ResolveEffect(effects[i], activity, permanent);
            }

            if (activity.Model.Id == "card_hunting" && permanent.Is(CardTag.Animal))
            {
                var reward = Get<DecisionSystem>().GetReward(activity);
                if (Get<TechSystem>().IsTechResearched("card_improve_hunting"))
                {
                    reward += 3f;
                }

                if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
                {
                    reward += reward;
                }

                Get<StatSystem>().AddFood(reward);
            }

            if (activity.Model.Id == "card_attack" && permanent.Is(CardTag.Animal))
            {
                var isWin = true;

//                if (permanent.Is(CardTag.Animal))
//                {
                    // Расчет битвы с хищниками
                    // отключаем по дизайну
//                    if (permanent.Is(CardTag.Aggressive) && permanent.Duration > 0)
//                    {
//                        var player = Get<StatSystem>().Army.CurValue;
//                        var enemy = permanent.Duration;
//
//                        if (player >= enemy && player > 0)
//                        {
//                            // игрок побеждает и уничтожает хищников
//                            var dmg = Mathf.RoundToInt(Mathf.Min(player, player * enemy / player));
//                            Get<StatSystem>().AddArmy(-dmg);
//                        }
//                        else
//                        {
//                            // игрок проигрывает
//                            Get<StatSystem>().AddArmy(-player);
//                            permanent.Duration -= Mathf.RoundToInt(Mathf.Min(enemy, enemy * player / enemy));
//
//                            isWin = false;
//                        }
//                    }

                    // Игрок получает награду за победу
//                    if (isWin)
//                    {
//                        var defense = Get<MapSystem>().GetProperty(permanent.Id, "card_tribe_defense");
//                        if (defense != null && defense.Duration > 0)
//                        {
//                            isWin = false;
//                            defense.Duration--;
//                            defense.UpdateViews();
//                        }
//                        else
//                        {
//                            var reward = 10f;   // львы и волки
//                            if (!permanent.Is(CardTag.Aggressive))
//                            {
//                                reward += 5f;   // козы и олени
//                            }
//
//                            if (Get<TechSystem>().IsTechResearched("card_improve_hunting"))
//                            {
//                                reward += 3f;
//                            }
//
//                            if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
//                            {
//                                reward += reward;
//                            }
//
//                            Get<StatSystem>().AddFood(reward);
//                        }
//                    }
//                }

                // армию племени удаляем в самой битве, выше
                if (isWin && !permanent.Is(CardTag.Tribe))
                {
                    Get<MapSystem>().DestroyPermanent(permanent);
                }
            }

            // Создаем рабочему занятие по умолчанию
            GameState.Current.Cards.Remove(activity);

//            var fakeExploring = activity.Model.Id == "card_exploring" && (permanent.Is(CardTag.Landscape) || location.Owner != "player");
//            var fakeAttack = (activity.Model.Id == "card_attack" || activity.Model.Id == "card_hunting" ||
//                              activity.Model.Id == "card_clean_landscape" ||
//                              activity.Is(CardTag.Diplomacy) || activity.Is(CardTag.Combat) ||
//                              activity.Model.Id == "card_clean" || activity.Model.Id == "card_excuse") && 
//                             location.Owner != "player";
//            var fakeExpansion = activity.Model.Id == "card_expansion" && location.Owner != "player";
//            if (activity.Model.Id == "card_abandon" || fakeExploring || fakeAttack || fakeExpansion)
//            {
//                Get<WorkerSystem>().KillWorkerAtLocation(worker.Location, true);
//            }
//            else
            {
                Get<DecisionSystem>().CreateWork(worker);
            }

            location.UpdateViews();
            await Task.Yield();
        }

        public void CancelActivity(WorkerContext worker, bool createWork = true)
        {
            var activity = GetActivityForWorker(worker.Id);
            if (activity == null)
            {
                return;
            }

            Debug.Log($"Cancel: {activity.Model.Id}");

            // Удаляем все модификаторы от активити
            Get<ModifiersSystem>().RemoveAllModifiers(activity.Id);

            // Вернуть перманент из активити обратно в локацию
            var permanent = GetPermanent(activity.Id);
            permanent.Owner = worker.Location;

            // уничтожаем активити
            GameState.Current.Cards.Remove(activity);

            // Создаем рабочему новое занятие по умолчанию (если было отменено не занятие)
            if (createWork && !activity.Is(CardTag.Work))
            {
                Get<DecisionSystem>().CreateWork(worker);
            }

            GameState.Current.Cards.Remove(activity);

            // обновить локацию
            var location = Get<MapSystem>().GetLocation(worker.Location);
            location.UpdateViews();
        }

        public float GetStatModifierByPermanent(string stat, uint location)
        { 
            var statMod = 0f;
            var techBonus = Get<TechSystem>().IsTechResearched("card_improve_crops") ? 1 : 0;

            // Берем тольк разведанные перманенты
            var permanents = Get<MapSystem>().GetPermanents(location)
                .Where(x => !x.Is(CardTag.Unexplored) && !x.Is(CardTag.Ruined))
                .ToList();

            var land = Get<MapSystem>().GetPermanents(location).FirstOrDefault(x => x.Is(CardTag.Landscape));
            if (land?.Is(CardTag.Ruined) == true)
            {
                permanents.RemoveAll(x => x.Is(CardTag.Building));
            }

            bool Has(string cardName)
            {
                return permanents.Any(x => x.Model.Id == cardName && (Get<MapSystem>().IsEnabledOnMap(x) || x.Is(CardTag.Building)));
            }

            // Пища
            if (stat == Stats.Food && Has("card_wheat"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Food && Has("card_grape") && (Has("card_field") || Has("card_pasture") || Has("card_irrigation")))
            {
                if (Has("card_field"))
                {
                    statMod += 1f;
                }
                if (Has("card_pasture"))
                {
                    statMod += 1f;
                }
                if (Has("card_irrigation"))
                {
                    statMod += 1f;
                }

                statMod += techBonus;

                if (Has("card_swamp"))
                {
                    statMod -= 1f;
                }
            }
            if (stat == Stats.Food && Has("card_fish"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Food && Has("card_river"))
            {
                statMod += 1f;
            }

            // Продукция
            if (stat == Stats.Production && Has("card_stone"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Production && Has("card_ore") && (Has("card_workshop") || Has("card_factory")))
            {
                statMod += 2f + techBonus;
            }
            if (stat == Stats.Production && Has("card_clay"))
            {
                statMod += 1f + techBonus;
            }

            // Культура
            if (stat == Stats.Culture && Has("card_sacred_place"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Culture && Has("card_arcane_place") && (Has("card_gymnasia") || Has("card_academy")))
            {
                statMod += 2f + techBonus;
            }

            // Золото
            if (stat == Stats.Gold && Has("card_gold"))
            {
                statMod += 1f + techBonus;
            }

            if (stat == Stats.Faith && Has("card_incense"))
            {
                statMod += 1f + techBonus;
            }

            // получить еду с домашних зверей
            // +1 еды с домашних зверей за каждую теху Зверовода
            techBonus = Get<TechSystem>().IsTechResearched("card_improve_hunting") ? 1 : 0;
            if (Get<GameEventSystem>().IsEventStarted("card_event_scouts"))
            {
                techBonus += 1;
            }

            if (stat == Stats.Food && permanents.Any(x => x.Model.Id == "card_boar" && x.Is(CardTag.Pet)))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Food && permanents.Any(x => x.Model.Id == "card_goat" && x.Is(CardTag.Pet)))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Food && permanents.Any(x => x.Model.Id == "card_cow" && x.Is(CardTag.Pet)))
            {
                statMod += 2f + techBonus;
            }

            if (stat == Stats.Army && permanents.Any(x => x.Model.Id == "card_wolf" && x.Is(CardTag.Pet)))
            {
                statMod += 1f;
            }


            if (permanents.Any(x => x.Model.Id == "card_cursed_place" && !x.Is(CardTag.Unexplored)))
            {
                statMod *= 0.5f;
            }


            // Постройки
            techBonus = Get<TechSystem>().IsTechResearched("card_improve_workshops") ? 1 : 0;
            if (stat == Stats.Production && Has("card_houses"))
            {
                statMod += 1f;
            }
            if (stat == Stats.Production && Has("card_huts"))
            {
                statMod += 1f;
            }
            if (stat == Stats.Food && Has("card_field"))
            {
                statMod += 1f + techBonus;
                
                if (Has("card_swamp"))
                {
                    statMod -= 1f;
                }
                if (permanents.Any(x => (x.Model.Id == "card_goat" || x.Model.Id == "card_deer") && !x.Is(CardTag.Pet)))
                {
                    statMod -= 1f;
                }
                if (statMod < 0f)
                {
                    statMod = 0f;
                }
            }
            if (stat == Stats.Food && Has("card_melioration"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Food && Has("card_pasture"))
            {
                statMod += 2f + techBonus;
            }
            if (stat == Stats.Production && Has("card_pasture"))
            {
                statMod += 1f + techBonus;
            }

            if (stat == Stats.Food && Has("card_cultivation"))
            {
                statMod += 2f;
            }
            if (stat == Stats.Food && Has("card_irrigation"))
            {
                statMod += 3f + techBonus;
            }

            if (stat == Stats.Production && Has("card_workshop"))
            {
                statMod += (1f + techBonus);
            }
            if (stat == Stats.Production && Has("card_factory"))
            {
                statMod += (2f + techBonus);
            }

            if (stat == Stats.Culture && Has("card_gymnasia"))
            {
                statMod += 1f + techBonus;
            }
            if (stat == Stats.Culture && Has("card_academy"))
            {
                statMod += 2f + techBonus;
            }

            if (stat == Stats.Army && Has("card_barracks"))
            {
                statMod += 1f;
            }
            if (stat == Stats.Gold && Has("card_port"))
            {
                statMod += 1f;
            }

            if (stat == Stats.Faith && Has("card_shrine"))
            {
                statMod += 1f;
            }

            // Во время Засухи добыча еды останавливается
            // TODO: от любых событий любым статам домножать?
//            if (stat == Stats.Food && Get<GameEventSystem>().IsEventStarted("card_event_drought"))
//            {
//                var mod = Get<ModifiersSystem>().GetGroupValue(Get<StatSystem>().Food.Id, ModifierGroup.FromEvents);
//                statMod *= (1f + mod);
//            }

            return statMod;
        }

        public float GetStatToTrade(string stat, uint location, string decision)
        {
            var usages = GameState.Current.GameContext.DecisionUsages
                             .FirstOrDefault(x => x.Card == decision)?.Amount ?? 0;
            if (GameState.Current.GameContext.Difficulty == DifficultyLevel.Hard)
            {
                usages += 2;
            }

            // торговля с купцами
            if (Get<MapSystem>().IsTrader(stat))
            {
                usages = GameState.Current.GameContext.DecisionUsages
                             .FirstOrDefault(x => x.Card == "_card_diplomacy")?.Amount ?? 0;
                if (GameState.Current.GameContext.Difficulty == DifficultyLevel.Hard)
                {
                    usages += 2;
                }
                return 15f + usages;
            }

            // бонус к ресурсам от племени
//            var tribeBonus = 0f;
//            if (Get<MapSystem>().GetPermanents(location).Any(x => x.Is(CardTag.Tribe)))
//            {
//                if (stat == Stats.Food || stat == Stats.Production)
//                {
//                    tribeBonus = 1f;
//                }
//            }

            return 12 + usages;
//                3f * (GetStatModifierByPermanent(stat, location) + tribeBonus) + usages;
        }
    }
}
