﻿namespace Assets.Game.Domain.Systems.Activities
{
    using Areas;
    using Components;
    using Core;
    using UI.Views;
    using Unity.Entities;
    using UnityEngine;
    using UnityEngine.Assertions;

    // Used to create tasks for clicked activity
    // or cancel current task

    [AlwaysUpdateSystem]
    [UpdateAfter(typeof(DashboardBarrier))]
    [UpdateInGroup(typeof(GuiUpdateGroup))]
    public class ActivityEventSystem : ComponentSystem
    {
        private struct AgentLocations
        {
            public readonly int Length;
            public ComponentDataArray<TileAgentTag> Tag;
            public ComponentDataArray<Link> Link;
        }

        private Entity _curTile;
        private Entity _curActivity;
        private Entity _newActivity;
        private Entity _curTask;
        private bool _activityChanged;
        private EntityManager _entityManager;

        [Inject] private ActivityEventBarrier _barrier;
        [Inject] private AgentLocations _locations;        

        [Inject] private DashboardViewSystem _dashboardViewSystem;
        [Inject] private MovingActivitySystem _movingActivitySystem;
        [Inject] private TileSystem _tileSystem;
        [Inject] private GatheringActivitySystem _gatheringActivitySystem;

        protected override void OnCreateManager()
        {
            _entityManager = World.Active.GetOrCreateManager<EntityManager>();
        }

        protected override void OnStartRunning()
        {
            base.OnStartRunning();

            Messenger.TileEvent += OnTileSelected;
            Messenger.ActivityViewEvent += OnActivityViewClick;
        }

        protected override void OnStopRunning()
        {
            Messenger.TileEvent -= OnTileSelected;
            Messenger.ActivityViewEvent -= OnActivityViewClick;

            base.OnStopRunning();
        }

        protected override void OnUpdate()
        {
            if (_curActivity == _newActivity)
            {
                return;
            }

            var buffer = _barrier.CreateCommandBuffer();

            if (_curActivity != Entity.Null)
            {
                // Check task before destroying (ex. it was completed and removed from outside)
                if (_entityManager.Exists(_curTask))
                {
                    // For Moving activity need to destroy destination
                    if (_entityManager.HasComponent<MovingActivityTag>(_curTask))
                    {
                        var destGroup = GetComponentGroup(typeof (TileAgentDestination), typeof (Link));
                        var entity = destGroup.GetEntityArray();

                        Assert.IsTrue(entity.Length == 1, $"Wrong destination amount for Moving");

                        buffer.DestroyEntity(entity[0]);

                        _tileSystem.ClearTimeMarks();
                        Messenger.Publish(MessageType.TileEvent, Entity.Null,
                            new TileEventArgs() {EventType = TileEventType.AgentDestinated});
                    }

                    // Cancel current activity
                    buffer.DestroyEntity(_curTask);
                }

                var group = GetComponentGroup(typeof(CancelActivityTag), typeof(AttachedTo));
                group.SetFilter(new AttachedTo() { Target = _curTask });
                var cancels = group.GetEntityArray();

                Assert.IsTrue(cancels.Length == 1, $"Missing Cancel activity for {_curTask}");

                buffer.DestroyEntity(cancels[0]);

                // TODO: Destroy all links for current activity

                _curTask = Entity.Null;
                _curActivity = Entity.Null;
            }

            if (_entityManager.HasComponent<CancelActivityTag>(_newActivity))
            {
                _curActivity = Entity.Null;
                _newActivity = Entity.Null;
                return;
            }

            // Create new activity
            var titleId = _entityManager.GetComponentData<Localization>(_newActivity).TitleId;

            _curTask = _entityManager.CreateEntity();
            buffer.AddComponent(_curTask, new TaskTag());
            buffer.AddComponent(_curTask, new Localization() { TitleId = titleId });
            buffer.AddComponent(_curTask, GameState.CreateExperience(1));

            // We have to force update injects after manager created new entity for task
            // To find agent without disposings
            UpdateInjectedComponentGroups();

            if (_entityManager.HasComponent<GatheringActivityTag>(_newActivity))
            {
                var agent = FindAgentAtTile(_curTile);

                var group = GetComponentGroup(typeof(Plant), typeof(AttachedTo));
                group.SetFilter(new AttachedTo() { Target = _curTile });
                var plants = group.GetEntityArray();
                var palmIndex = -1;
                for (int i = 0; i < plants.Length; i++)
                {
                    if (_entityManager.GetComponentData<Plant>(plants[i]).Id == 402)
                    {
                        palmIndex = i;
                        break;
                    }
                }
                Assert.IsFalse(palmIndex == -1, "failed to find palm");
                
                var taskDuration = _gatheringActivitySystem.GetGatheringDuration(agent, plants[palmIndex]);
                buffer.AddComponent(_curTask, new TaskDuration() { Value = taskDuration, Total = taskDuration });
                buffer.AddSharedComponent(_curTask, new GatheringActivityTag());
                buffer.AddSharedComponent(_curTask, new AttachedTo() { Target = agent });
                buffer.AddComponent(_curTask, new Link() { Source = agent, Target = plants[palmIndex] });
            }

            if (_entityManager.HasComponent<MovingActivityTag>(_newActivity))
            {
                // TODO: Its working if player has only one agent
                var index = FindAgentLocation();
                var agent = _locations.Link[index].Source;
                var tile = _locations.Link[index].Target;

                var path = _tileSystem.GetPath(tile, _curTile);
                Debug.Log("PATH: " + path.Length);
                Assert.IsTrue(path.Length >= 2, $"Invalid path");
                _tileSystem.ClearTimeMarks();

                var totalTime = 0;
                var breakTime = 0;
                var firstTileAtPath = path.Length - 2;
                for (int i = 0; i < path.Length - 1; i++)
                {
                    var t = _movingActivitySystem.CalculateDuration(path[i]);
                    _tileSystem.ShowTimeTest(path[i], t.ToString());

                    totalTime += t;

                    if (i == firstTileAtPath)
                    {
                        breakTime = t;
                    }
                }
                
                buffer.AddComponent(_curTask, new TaskDuration() { Value = totalTime, Total = totalTime, Break = breakTime });
                buffer.AddSharedComponent(_curTask, new MovingActivityTag());
                buffer.AddComponent(_curTask, new Link() { Source = agent, Target = path[firstTileAtPath] });   // Set first tile at path
                buffer.AddSharedComponent(_curTask, new AttachedTo() { Target = agent });

                path.Dispose();

                // Create destination
                var destination = _entityManager.CreateEntity();
                buffer.AddComponent(destination, new TileAgentDestination());
                buffer.AddComponent(destination, new Link() { Source = agent, Target = _curTile });

                Messenger.Publish(MessageType.TileEvent, _curTile, new TileEventArgs() { EventType = TileEventType.AgentDestinated });
            }

            // TODO: Add all links for Gathering

            var cancel = _entityManager.CreateEntity();
            buffer.AddSharedComponent(cancel, new ActivityTag() { Id = 190 });
            buffer.AddComponent(cancel, new Localization() { TitleId = 190 });
            buffer.AddSharedComponent(cancel, new CancelActivityTag());
            buffer.AddSharedComponent(cancel, new AttachedTo() { Target = _curTask });

            Debug.Log("CREATED");
            
            _curActivity = _newActivity;
        }

        private Entity FindAgentAtTile(Entity tile)
        {
            for (int i = 0; i < _locations.Length; i++)
            {
                if (_locations.Link[i].Target == tile)
                {
                    return _locations.Link[i].Source;
                }
            }

            return Entity.Null;
        }
        private int FindAgentLocation()
        {
            for (int i = 0; i < _locations.Length; i++)
            {
                if (_locations.Link[i].Source != Entity.Null)
                {
                    return i;
                }
            }

            return -1;
        }

        private void OnTileSelected(object sender, TileEventArgs args)
        {
            if (args.EventType != TileEventType.Selected)
            {
                return;
            }

            _curTile = (Entity)sender;            
        }

        private void OnActivityViewClick(object sender, PointerEventArgs args)
        {
            if (args.EventType == PointerEventType.Clicked)
            {
                var source = (Entity)sender;

                if (!_entityManager.HasComponent<ActivityTag>(source) ||
                    _entityManager.HasComponent<OpenCloseContainerActivityTag>(source))
                {
                    return;
                }

                if (source == _curActivity)
                {
                    return;
                }

                _newActivity = source;

                _dashboardViewSystem.ClearActivities();
                TooltipView.Hide();
            }
        }

        public void OnTaskFinished(Entity task)
        {
            if (_curTask == task)
            {
                _curActivity = Entity.Null;
                _newActivity = Entity.Null;
            }
        }
    }

    [UpdateAfter(typeof(ActivityEventSystem))]
    [UpdateInGroup(typeof(GuiUpdateGroup))]
    public class ActivityEventBarrier : BarrierSystem
    {
    }
}
