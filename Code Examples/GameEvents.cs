﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IGameEvent
{
    Hashtable Tokens { get; set; }
    string Message { get; }
}

public class InformationEvent : IGameEvent
{
    public Hashtable Tokens { get; set; }

    private readonly string _message;

    private InformationEvent()
    {
    }

    public static EventBuilder Create()
    {
        return new EventBuilder(new InformationEvent());
    }

    public string Message { get { return Compile(_message); }}

    private string Compile(string text)
    {
        return Tokens.Cast<DictionaryEntry>()
            .Aggregate(text, (current, token) => current.Replace("[" + token.Key + "]", token.Value.ToString()));
    }
}

public class EventBuilder
{
    private readonly IGameEvent _event;

    public EventBuilder(IGameEvent newEvent)
    {
        _event = newEvent;
    }

    public IGameEvent ToGameEvent()
    {
        return _event;
    }

    public EventBuilder WithMessage(string message)
    {

        return this;
    }
}

public class EventTrigger : IEquatable<EventTrigger>
{
    public string Name { get; private set; }

    private EventTrigger()
    {
    }

    public static EventTrigger UnitInjured
    {
        get { return new EventTrigger() { Name = "UnitInjured" }; }
    }

    public bool Equals(EventTrigger other)
    {
        return other.Name == this.Name;
    }
}

public class EventFactory
{
    private readonly Dictionary<EventTrigger, List<Func<IGameEvent>>> _events = new Dictionary<EventTrigger, List<Func<IGameEvent>>>()
    {
        {EventTrigger.UnitInjured, new List<Func<IGameEvent>>()
        {
            () => InformationEvent.Create().ToGameEvent(),
        }},
    };

    public List<IGameEvent> CreateEvents(EventTrigger trigger)
    {
        if (_events.ContainsKey(trigger))
        {
            return new List<IGameEvent>(_events[trigger].Select(x => x.Invoke()));
        }

        throw new ArgumentException("Trigger not defined: " + trigger.Name);
    }
}