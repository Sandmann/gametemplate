﻿namespace Assets.Game.Domain.Models.Events
{
    using System;
    using Systems;
    using Core.Common;
    using UI;
    using UnityEngine;

    [CreateAssetMenu(fileName = "ArabicDivide", menuName = "Data/GameEvent/ArabicDivide")]
    public class ArabicDivide : GameEvent
    {
        public override void ShowEffect(out string title, out string text, out Action<EventActionView>[] actions)
        {
            var localization = GameSystem.Get<LocalizationSystem>();
            var game = GameState.Current.GameContext;

            game.IsArabicDivided = true;
            
            DebufCity("mecca");
            DebufCity("cordoba");
            DebufCity("baghdad");
            DebufCity("cairo");

            title = localization.ReplaceTags($"<color=green>{localization.Get("event_111_title")}</color>");
            text = localization.Get("event_111_descr");

            actions = null;
        }

        private void DebufCity(string cityName)
        {
            var diplomacy = GameSystem.Get<DiplomacySystem>();
            var city = diplomacy.FindCityContext(cityName);
            if (city == null)
            {
                //UnityEngine.Debug.LogError($"[ArabicDivide] Failed to find city '{cityName}'");
                return;
            }

            //если город уже захвачен, к нему не применяются эффекты от окончания ивента Arabic Divide
            if (city.Status == CityStatus.Colony)
            {
                return;
            }

            diplomacy.AddCityRelationToPlayer(city, 3.9f - city.RelationToPlayer);
            city.Army.Value *= 0.3f;
        }
    }
}
