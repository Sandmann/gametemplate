﻿namespace Assets.Game.Domain.Systems
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Contexts;
    using Core.Common;
    using Extensions;
    using Models;
    using Models.Events;
    using UI;
    using UnityEngine;
    using Random = UnityEngine.Random;

    [Serializable]
    public class GameEventSystem : ContextSystem<GameEventContext>
    {
#pragma warning disable 649
        [SerializeField] private EventIconView _eventIconPrefab;
        [SerializeField] private RectTransform _eventIconsRoot;

        [SerializeField] private EventBattleView _eventBattle;
        [SerializeField] private EventBattleResultView _eventBattleResult;
        [SerializeField] private EventBigDialogView _eventBigDialog;
        [SerializeField] private EventDialogView _eventDialog;
        [SerializeField] private WelcomeView _welcomeView;
        [SerializeField] private FinalScreenView _finalView;
        [SerializeField] private EventUnionView _unionView;

        [SerializeField] private GameEvent[] _events = new GameEvent[0];
#pragma warning restore 649

        private Queue<GameEventContext> _eventQueue = new Queue<GameEventContext>();
        private StatSystem _stats = new StatSystem();

        // события без иконки эффекта; только для показа диалога и работы с ним; не сохраняются
        private List<GameEventContext> _nonEffectEvents = new List<GameEventContext>();

        public override void Initialize()
        {
            Add(this);

            _eventDialog.Hide();
            _eventBattle.Hide();
            _eventBattleResult.Hide();
            _eventBigDialog.Hide();

            _eventIconsRoot.DestroyChildren();
            var events = GameState.Current.Events.ToArray();
            foreach (var item in events)
            {
                item.RestoreModel(_events.FirstOrDefault(x => item.ModelId == x.name));

                var view = GetFreeEffectIconView();
                view.Attach(item);
            }
            OrderEventIcons();

            _stats = Get<StatSystem>();
            
            _welcomeView.Hide();
            _finalView.Hide();
            _unionView.Hide();
        }

        public void ShowWelcomeScreen()
        {
            _welcomeView.Show();
        }

        public void ShowFinalScreen(FinalResult result)
        {
            GameScene.Instance.StartCoroutine(ExecuteAfterQueueCleaned(() =>
            {
                _finalView.Show(result);
            }));
        }
        public void ShowUnionScreen()
        {
            GameScene.Instance.StartCoroutine(ExecuteAfterQueueCleaned(() =>
            {
                _unionView.Show();
            }));
        }

        private IEnumerator ExecuteAfterQueueCleaned(Action callback)
        {
            yield return new WaitForSeconds(0.1f);

            while (EventDialogOpened() || _eventQueue.Count > 0)
            {
                yield return null;
            }

            callback();
        }

        public bool IsEventStarted(string eventId)
        {
            var model = FindEventModel(eventId);
            if (!model)
            {
                return false;
            }

            return FindContext(model) != null;
        }

        public GameEvent FindEventModel(string eventId)
        {
            var model = _events.FirstOrDefault(x => x.name == eventId)
                        ?? _events.FirstOrDefault(x => x.Id == eventId);

            if (!model)
            {
                Debug.LogError($"Unsupported event: {eventId}");
            }

            return model;
        }

        private void RunEvent(GameEventContext ctx)
        {
            var view = GetFreeEffectIconView();
            view.Attach(ctx);

            if (ctx.Model.Id == "23") //MinoanEruption
            {
                GameScene.Instance.ShakeGroundEffect();
            }

            view.Show();

            ctx.Model.RunEffect();
            if (ctx.Model.ShowEffectOnStart())
            {
                ShowEffect(ctx);
            }
            else
            {
                ShowEventDialog(ctx);
            }

            OrderEventIcons();
        }

        private void OrderEventIcons()
        {
            // OrderBy duration; max duration at the top
            var ordered = GameState.Current.Events
                .OrderBy(x => x.Duration.Value)
                .ToArray();
            for (int i = 0; i < ordered.Length; i++)
            {
                FindView(ordered[i])?.SetAsFirstSibling();
            }
        }

        public GameEventContext RunEventInQueue(string eventId)
        {
            var model = FindEventModel(eventId);
            if (!model)
            {
                return null;
            }

            var gameEvent = FindContext(model);
            if (gameEvent != null)
            {
                gameEvent.Duration.Value += model.Duration;
                OrderEventIcons();
            }
            else
            {
                gameEvent = (GameEventContext) CreateContext(model);
                RunEventInQueue(gameEvent);
            }

            return gameEvent;
        }

        private void RunEventInQueue(GameEventContext ctx)
        {
            if (EventDialogOpened())
            {
                ctx.QueueMode = EventQueueMode.RunEvent;
                _eventQueue.Enqueue(ctx);
                return;
            }

            RunEvent(ctx);
        }

        private EventIconView GetFreeEffectIconView()
        {
            for (int i = 0; i < _eventIconsRoot.childCount; i++)
            {
                var view = _eventIconsRoot.GetChild(i).GetComponent<EventIconView>();
                if (view.GetContext()?.Completed.Value == true || !view.gameObject.activeSelf)
                {
                    return view;
                }
            }

            return GameObject.Instantiate(_eventIconPrefab, _eventIconsRoot);
        }

        private Transform FindView(GameEventContext ctx)
        {
            for (int i = 0; i < _eventIconsRoot.childCount; i++)
            {
                var view = _eventIconsRoot.GetChild(i).GetComponent<EventIconView>();
                if (ctx.Id.Equals(view.GetContext()?.Id))
                {
                    return view.transform;
                }
            }

            return null;
        }

        /// <summary>
        /// Показать диалог события при его запуске (автоматически)
        /// </summary>
        /// <param name="ctx"></param>
        public void ShowEventDialog(GameEventContext ctx)
        {
            if(ctx.Model.ShowDialog(out string title, out string text))
            {
                Get<TutorialSystem>().HideAllHints();

                _eventDialog.Attach(ctx);
                _eventDialog.Show(title, text);

                Get<SoundSystem>().PlayEventSound(ctx.Model);
            }
        }

        public void ShowEffect(GameEventContext ctx)
        {
            Get<TutorialSystem>().HideAllHints();

            ctx.Model.ShowEffect(out string title, out string text, out Action<EventActionView>[] actions);

            if (actions == null)
            {
                _eventDialog.Attach(ctx);
                _eventDialog.Show(title, text);
            }
            else
            {
                if (ctx.Model.IsBattleEvent)
                {
                    _eventBattle.Attach(ctx);
                    _eventBattle.Show(title, text, actions);
                }
                else
                {
                    _eventBigDialog.Attach(ctx);
                    _eventBigDialog.Show(title, text, actions);
                }
            }

            Get<SoundSystem>().PlayEventSound(ctx.Model);
        }

        /// <summary>
        /// Показывает диалог события (без создания иконки-эффекта для него и сохранения в GameState)
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="setupParams"></param>
        public void ShowEventInQueue(string eventId)
        {
            var model = FindEventModel(eventId);
            if (!model)
            {
                return;
            }

            var ctx = _nonEffectEvents.FirstOrDefault(x => x.Model == model);
            if (ctx == null)
            {
                var eventCtx = FindContext(model);

                ctx = new GameEventContext(model);
                _nonEffectEvents.Add(ctx);

                // Если показываем копию реального события (например в конце сражения с городом, ставим длительность события реальную)
                // Это нужно, если событие добавляется в очередь
                if (eventCtx != null)
                {
                    ctx.Duration.Origin = eventCtx.Duration.Value;
                }
            }

            ShowEventInQueue(ctx);
        }

        // Возвращает список активных событий в данный ход
        public string[] ActiveEvents => GameState.Current.Events
            .Where(x => !x.Completed.Value)
            .Union(_nonEffectEvents)
            .Select(x => $"{x.Model.name} ({x.Model.Id})")
            .ToArray();

        private void ShowEventInQueue(GameEventContext ctx)
        {
            if (EventDialogOpened())
            {
                ctx.QueueMode = EventQueueMode.ShowEvent;
                _eventQueue.Enqueue(ctx);
                return;
            }

            ShowEffect(ctx);
        }

        /// <summary>
        /// Уничтожает одноразовое событие, которое просто показали (например Основание колонии)
        /// </summary>
        /// <param name="ctx"></param>
        public void DestroyEvent(GameEventContext ctx)
        {
            // Закрыли диалог без контекста события (при старте сцены например)
            if (ctx == null)
            {
                return;
            }

            _nonEffectEvents.Remove(ctx);

            // TODO: Destroy ctx and free resources after dialog will be closed
        }

        // checkFirstEvents
        public void RunNewGameEvents()
        {
            if (GameState.Current.GameContext.Model.CityType == CityType.Culture)
            {
                RunEventInQueue("UnionAttika");
            }
            if (GameState.Current.GameContext.Model.CityType == CityType.War)
            {
                RunEventInQueue("SourceOfSlaves");
            }
            if (GameState.Current.GameContext.Model.CityType == CityType.Trade)
            {
                RunEventInQueue("UsefulConnections");
            }

            //commented by design
            //if (!GameState.Current.GameContext.IsFree && !GameState.Current.GameContext.IsDemo)
            //{
            //    RunEventInQueue("CivPoints");
            //}
        }

        public override IContext FindContext(ContextId id)
        {
            throw new NotImplementedException();
        }

        public GameEventContext FindContext(GameEvent model)
        {
            return GameState.Current.Events.FirstOrDefault(x => x.Model == model) ??
                _nonEffectEvents.SingleOrDefault(x => x.Model == model);
        }

        public override IContext CreateContext(ScriptableObject model)
        {
            var ctx = new GameEventContext(model as GameEvent);
            GameState.Current.Events.Add(ctx);

            return ctx;
        }

        public override void Update()
        {
            if (GameState.Current.Blackboard.ArabicRaidDelay > 0)
            {
                GameState.Current.Blackboard.ArabicRaidDelay--;
            }

            if (GameState.Current.Events.Any(x => x == null))
            {
                UnityEngine.Debug.LogError("Null event!");
            }

            var events = GameState.Current.Events
                .Where(x => !x.Completed.Value)
                .ToArray();
            foreach (var gameEvent in events)
            {
                // Update event duration
                if (gameEvent.Duration.Value > 0 && gameEvent.Duration.Value < 1000)
                {
                    gameEvent.Duration.Value -= 1;

                    if (gameEvent.Duration.Value == 0)
                    {
                        EndEvent(gameEvent);
                    }
                    else
                    {
                        if (gameEvent.Model == null)
                        {
                            UnityEngine.Debug.LogError("Game event model null!");
                        }

                        gameEvent.Model.UpdateEventEffect();
                    }

                    continue;
                }

                if (gameEvent.Model == null)
                {
                    UnityEngine.Debug.LogError("Game event model null!");
                }

                // Try end event by condition
                if (gameEvent.Model.CanFinish())
                {
                    ShowResultInQueue(gameEvent); // action => success
                }
            }
            
            GameState.Current.Events.RemoveAll(x => x.Completed.Value);

            var game = GameState.Current.GameContext;
            game.WithoutSigesTurns += 1;    // город живет в мире без штурмов

            // Проверка на голод
            ValidateHunger();

            // Проверка на банкротство
            ValidateGoldCollapse();
            
            // Проверка на наличие новых атак
            ValidateEnemyActions();

            ValidateMigration();
            ValidateDomination();

            // Обновляем эффект Христианства
            if (IsEventStarted("RiseOfCristians"))
            {
                var ev = (RiseOfCristians) FindEventModel("RiseOfCristians");
                ev.UpdateEffect();
            }

            if (game.Turn.Value == 15)
            {
                RunEventInQueue("MinoanEruption");
            }

            if (game.TurnUnscaled == 18 && game.Model.CityType == CityType.War)
            {
                RunEventInQueue("Hilon");
            }

            if (game.Turn.Value == 20)
            {
                RunEventInQueue("DorianInvasion");

                Get<TutorialSystem>().ShowEventHints();
            }

            if (game.Turn.Value == 42)
            {
                RunEventInQueue("PersianInvasion");
            }

            if (game.Turn.Value == 44 && game.Model.CityType == CityType.Trade)
            {
                RunEventInQueue("Periandr");
            }

            if (game.Turn.Value == 45 && game.Model.CityType == CityType.Culture)
            {
                RunEventInQueue("Solon");
            }

            if (game.Turn.Value == 57)
            {
                RunEventInQueue("MarchEastward");
            }

            if (game.Turn.Value >= 53 && Config.IsDemo)
            {
                ShowEventInQueue("CompleteDemo");
                return;
            }

            if (game.Turn.Value == 80)
            {
                RunEventInQueue("RiseOfRome");
            }

            if (game.Turn.Value == 100)
            {
                ShowEventInQueue("NewAgeBegins");
            }

            if (game.Turn.Value == GameState.SKIFS_START_TURN && !game.IsFree)
            {
                RunEventInQueue("ScythiansInvade");
            }

            if (game.Turn.Value == 145 && !game.IsFree)
            {
                RunEventInQueue("RiseOfCristians");
            }

            if (game.Turn.Value == GameState.CHRIST_TURN_STAGE1 && IsEventStarted("RiseOfCristians") && !game.IsFree)
            {
                ShowEventInQueue("ChristiansInfluence");
            }
            if (game.Turn.Value == GameState.CHRIST_TURN_STAGE2 && IsEventStarted("RiseOfCristians") && !game.IsFree)
            {
                ShowEventInQueue("ChristiansInfluence");
            }
            if (game.Turn.Value == GameState.CHRIST_TURN_STAGE3 && IsEventStarted("RiseOfCristians") && !game.IsFree)
            {
                ShowEventInQueue("ChristiansInfluence");
            }

            if (game.Turn.Value == GameState.HUNS_START_TURN)
            {
                RunEventInQueue("HunnsInvade");
            }

            // Раскол Римской Империи
            if (game.Turn.Value == 179 && IsEventStarted("RomeEmpire"))
            {
                ShowEventInQueue("RomeDivide");

                if (!IsEventStarted("UnionGreeceBizantia") && game.OpenedMapId.Value == 1)
                {
                    RunEventInQueue("UnionGreeceBizantia");
                }
            }
            
            // Запустить восстановление Рима
            if (!IsEventStarted("RestoreOfRome") && 
                !IsEventStarted("TradePartners") &&
                GameState.Current.Blackboard.IsRomeFailed &&
                !GameState.Current.Blackboard.RomeRestored &&
                game.Model.CityType == CityType.Culture)
            {
                RunEventInQueue("RestoreOfRome");
            }

            // Запустить восстановление Рима для Коринфа
            if (!IsEventStarted("RestoreOfRome") &&
                !IsEventStarted("RiskyInvestments") &&
                GameState.Current.Blackboard.IsRomeFailed &&
                !GameState.Current.Blackboard.RomeRestored &&
                game.Model.CityType == CityType.Trade)
            {
                RunEventInQueue("RestoreOfRome");
            }

            if (game.Turn.Value == GameState.ROME_FAILING_TURN_START)
            {
                if (IsEventStarted("RomeEmpire"))
                {
                    EndEvent(FindContext(FindEventModel("RomeEmpire")));
                }

                GameState.Current.Blackboard.IsRomeFailed = true;

                ShowEventInQueue("RomeLegacy");
                ShowEventInQueue("RomeLegacyMenu");
            }

            if (game.Turn.Value == GameState.PLAGUE_START_TURN)
            {
                RunEventInQueue("Plague");
            }

            if (game.Turn.Value == GameState.ARABS_START_TURN)
            {
                ShowEventInQueue("ArabicRise");
                RunEventInQueue("ArabicInvade");
            }

            if (game.Turn.Value == GameState.CORDOBA_FOUNDATION_TURN ||
                game.Turn.Value == GameState.BAGHDAD_FOUNDATION_TURN ||
                game.Turn.Value == GameState.CAIRO_FOUNDATION_TURN)
            {
                ShowEventInQueue("ArabicImprove");
            }

            if (game.Turn.Value == GameState.ARABS_DIVIDED_TURN)
            {
                ShowEventInQueue("ArabicDivide");
            }

            if (game.Turn.Value == 290 || (game.Turn.Value == 150 && game.IsFree))
            {
                ShowEventInQueue("EndGameWarning");
            }

            if (game.Turn.Value >= GameState.ARABS_DIVIDED_TURN &&
                !IsEventStarted("ConquerWorld") &&
                !IsEventStarted("TradePartners") &&
                !IsEventStarted("RestoreOfRome") &&
                GameState.Current.Blackboard.WorldDomination < 100)
            {
                RunEventInQueue("ConquerWorld");
            }

            // TODO: проверять unscaled game turn?
            if (game.Turn.Value >= game.WinTurns[4] && game.WinInTurn.Value == 0)
            {
                if (!game.FinalRewardAchieved && !game.IsFree && !Config.IsDemo)
                {
                    //не отображать событие с текстом об очках цивилизации т.к. их убрали
                    //ShowEventInQueue("EndGame");
                    
                    game.FinalRewardAchieved = true;
                }

                ShowFinalScreen(game.IsFree ? FinalResult.Defeat : FinalResult.Victory);
            }
            
            if (game.Turn.Value == 25 || game.Turn.Value == 36 || game.Turn.Value == 47 || game.Turn.Value == 61 || game.Turn.Value == 74 ||
                game.Turn.Value == 86 || game.Turn.Value == 96 || game.Turn.Value == 110 || game.Turn.Value == 119 || game.Turn.Value == 137 ||
                game.Turn.Value == 145 || game.Turn.Value == 157 || game.Turn.Value == 175 || game.Turn.Value == 182 || game.Turn.Value == 202 ||
                game.Turn.Value == 232 || game.Turn.Value == 242 || game.Turn.Value == 252 || game.Turn.Value == 262 || game.Turn.Value == 276 ||
                game.Turn.Value == 283 || game.Turn.Value == 295 || game.Turn.Value == 300 || game.Turn.Value == 320 || game.Turn.Value == 340 ||
                game.Turn.Value == 360 || game.Turn.Value == 380 || game.Turn.Value == 400 || game.Turn.Value == 450 || game.Turn.Value == 500)
            {
                // Сначала пробуем запустить специальные случайные события по городам с более высоким шансом
                if (!TryRunSpecialRandomEvent())
                {
                    ShowEventInQueue("RandomEvent");
                }
            }

            if (!GameState.Current.GameContext.TutorialEnabled)
            {
                // Пробуем запустить дипломатические события
                ValidateDiplomacyEvents();
            }

            if (game.WithoutSigesTurns > 32)
            {
                game.WithoutSigesTurns = 0;
                ShowEventInQueue("PeaceAges"); //Если нет осад на город игрока больше чем 32 ходов, выбираем бонус за мирное развитие
            }
        }

        private void ValidateDiplomacyEvents()
        {
            var game = GameState.Current.GameContext;
            var blackboard = GameState.Current.Blackboard;

            game.DiplomacyEventTurns++;
            var cities = GameState.Current.Cities
                .Where(x => x.IsActiveIndependent && x.Explored.Value)
                .ToArray();
            if (cities.Length > 0 && game.DiplomacyEventTurns >= GameState.DiplomacyEventDelay)
            {
                game.DiplomacyEventTurns = 0;

                var bad = cities.Count(x => x.RelationToPlayer < 3f) / cities.Length;
                var neutral = cities.Count(x => x.RelationToPlayer >= 3f && x.RelationToPlayer < 7f) / cities.Length;
                var good = cities.Count(x => x.RelationToPlayer >= 7f) / cities.Length;
                var rnd = Random.value;
                if (rnd < bad)
                {
                    var city = cities
                        .Where(x => x.RelationToPlayer < 3f)
                        .ToArray()
                        .GetRandom();

                    rnd = Random.value;
                    if (rnd <= 0.5f)
                    {
                        blackboard.InsultCityId = city.Model.Id;
                        ShowEventInQueue("Insult");
                    }
                    else
                    {
                        blackboard.DiplomacyCultureChangeCityId = city.Model.Id;
                        ShowEventInQueue("DiplomacyEnemyRaid");
                    }
                }
                else if (rnd < bad + neutral)
                {
                    var marriages = cities
                        .Where(x => x.RelationToPlayer >= 3f && x.RelationToPlayer < 7f)
                        .ToList();
                    
                    rnd = Random.value;
                    if (rnd <= 0.5f)
                    {
                        blackboard.DiplomacyCultureChangeCityId = marriages.GetRandom().Model.Id;
                        RunEventInQueue("DiplomacyIncident");
                    }
                    else
                    {
                        // убрать все города от которых может быть бонус армии
                        // в случае если армия еще не анлокнута
                        if (_stats.Army.MaxValue.Origin == 0)
                        {
                            marriages.RemoveAll(x => x.Model.CityType == CityType.War);
                        }

                        blackboard.DiplomacyMarriageCities = new string[4];
                        for (int i = 0; i < 4 && i < marriages.Count; i++)
                        {
                            var city = marriages.GetRandom();
                            blackboard.DiplomacyMarriageCities[i] = city.Model.Id;
                            marriages.Remove(city);
                        }

                        // не запускать событие если нет городов
                        if (blackboard.DiplomacyMarriageCities.Any(x => !string.IsNullOrEmpty(x)))
                        { 
                            RunEventInQueue("DiplomacyMarriage");
                        }
                    }
                }
                else if (good > 0f)
                {
                    blackboard.DiplomacyCultureChangeCityId = cities
                        .Where(x => x.RelationToPlayer >= 7f)
                        .ToArray()
                        .GetRandom().Model.Id;

                    rnd = Random.value;
                    if (rnd <= 0.5f)
                    {
                        ShowEventInQueue("DiplomacyCultureChange");
                    }
                    else
                    {
                        ShowEventInQueue("DiplomacyGifts");
                    }
                }
            }
        }

        private bool TryRunSpecialRandomEvent()
        {
            var game = GameState.Current.GameContext;

            if (Random.value < 0.4f)
            {
                if (game.Model.CityType == CityType.Trade)
                {
                    game.IsLastEventNegative = false;

                    var gold = Mathf.FloorToInt(5f * _stats.Gold.Growth.Value);
                    if (Random.value < 0.5f && _stats.Gold.CurValue.Value > gold && gold > 0 && !IsEventStarted("CreditFromPlayer"))
                    {
                        GameState.Current.Blackboard.RequestCreditGold[0] = gold;
                        RunEventInQueue("RequestCredit");
                        return true;
                    }

                    ShowEventInQueue("WealthyAge");
                    return true;
                }

                if (game.Model.CityType == CityType.War)
                {
                    if (Random.value < 0.5f && !IsEventStarted("WinnerGlory"))
                    {
                        game.IsLastEventNegative = false;

                        ShowEventInQueue("GrandGeneral");
                        RunEventInQueue("WinnerGlory");
                        return true;
                    }
                    
                    if (!IsEventStarted("SlavesHuntReward"))
                    {
                        game.IsLastEventNegative = false;

                        RunEventInQueue("SlavesHunt");
                        return true;
                    }
                }

                if (game.Model.CityType == CityType.Culture)
                {
                    if (Random.value < 0.75f && !IsEventStarted("GrandPhilosopher"))
                    {
                        game.IsLastEventNegative = false;

                        RunEventInQueue("GrandPhilosopher");
                        return true;
                    }

                    if (!IsEventStarted("Walkout"))
                    {
                        game.IsLastEventNegative = true;
                        RunEventInQueue("Walkout");
                        return true;
                    }
                }
            }

            return false;
        }

        public void TryCompleteEvent(string eventId, bool forced = false)
        {
            var model = FindEventModel(eventId);
            if (model == null)
            {
                return;
            }

            if (IsEventStarted(eventId) && (forced || model.CanFinish()))
            {
                ShowResultInQueue(FindContext(model));
            }
        }

        public void OnCityConquered()
        {
            TryCompleteEvent("FirstPurchases");
            TryCompleteEvent("BuyingGreece");
            TryCompleteEvent("GreeceConquest");
            TryCompleteEvent("GreeceHegemon");
            TryCompleteEvent("SourceOfSlaves");
        }

        // combatStartEndTurnCheck
        private void ValidateEnemyActions()
        {
            var game = GameState.Current.GameContext;
            if (game.EnemyAttackLifetime > 0)
            {
                game.EnemyAttackLifetime--;
                return;
            }

            if (game.CoalitionAttack.Value <= game.AllianceDefense.Value)
            {
                return; // Нужно 100% превосходства армий коалиций для начала атаки
            }

            RunEventInQueue("EnemyAttacksPlayer");
        }

        private void ValidateGoldCollapse()
        {
            if (Get<StatSystem>().Gold.CurValue.Value >= 0)
            {
                return;
            }

            if (GameState.Current.GameContext.Model.CityType == CityType.Culture ||
                GameState.Current.GameContext.Model.CityType == CityType.Trade)
            {
                var model = (GoldCollapse)FindEventModel("GoldCollapse");
                if (!model.HasAnyAction)
                {
                    ShowFinalScreen(FinalResult.Defeat);
                    return;
                }

                ShowEventInQueue("GoldCollapse");
            }

            if (GameState.Current.GameContext.Model.CityType == CityType.War)
            {
                ShowEventInQueue("GoldCollapseSparta");
            }
        }

        private void ValidateHunger()
        {
            var game = GameState.Current.GameContext;
            if (game.HungerLevel.Value >= game.HungerLimit.Value)
            {
                if (game.Model.CityType == CityType.Culture || game.Model.CityType == CityType.Trade)
                {
                    ShowEventInQueue("HungerRiot");
                }
                if (game.Model.CityType == CityType.War)
                {
                    ShowEventInQueue("HungerRiotSparta");
                }

                game.HungerLevel.Value = 0;
            }

            if (IsEventStarted("OverPopulation") && Get<StatSystem>().Population.CurValue.Value < game.OverPopulation)
            {
                EndEvent(FindContext(FindEventModel("OverPopulation")));
                game.OvergrowthModifier.Value = 0f;
            }

            if (Get<StatSystem>().Population.CurValue.Value >= game.OverPopulation)
            {
                game.OverPopulationDebuff = Get<StatSystem>().Population.CurValue.Value / 50000f;
                game.OvergrowthModifier.Value = game.OverPopulationDebuff * 0.02f * GameScene.PEOPLE_EATING_BASE;

                if (!IsEventStarted("OverPopulation"))
                {
                    RunEventInQueue("OverPopulation");
                }
            }
        }

        private void ValidateMigration()
        {
            var game = GameState.Current.GameContext;
            var blackboard = GameState.Current.Blackboard;
            var stats = Get<StatSystem>();

            if (blackboard.MigrationTurn + blackboard.MigrationDelay > game.Turn.Value)
            {
                return;
            }

            var fields = Get<LocationSystem>().FindContext("fields");
            if (fields.PopulationLimit < 1.5f * stats.Population.CurValue.Value)
            {
                return; //Макс знач рабочих на панели еды больше чем населения*1.5
            }

            if (stats.Population.MaxValue.Value < 1.5f * stats.Population.CurValue.Value)
            {
                return; //Население *1.5 меньше чем максимально населения.
            }

            if (IsEventStarted("Plague"))
            {
                return; //Во время чумы их не должно быть 
            }

            blackboard.MigrationSize = 2;
            if (fields.PopulationLimit > stats.Population.CurValue.Value)
            {
                blackboard.MigrationSize = Mathf.FloorToInt(fields.PopulationLimit - stats.Population.CurValue.Value) / 2;
                if (blackboard.MigrationSize > stats.Population.MaxValue.Value - stats.Population.CurValue.Value)
                {
                    blackboard.MigrationSize = Mathf.FloorToInt(stats.Population.MaxValue.Value - stats.Population.CurValue.Value);
                }

                if (blackboard.MigrationSize > 20000)
                {
                    blackboard.MigrationSize = fields.PopulationLimit / 3;
                }
            }

            if (blackboard.MigrationSize == 0)
            {
                return;
            }

            blackboard.MigrationTurn = game.Turn.Value;
            ShowEventInQueue("Migration");
        }

        private void ValidateDomination()
        {
            var game = GameState.Current.GameContext;
            var blackboard = GameState.Current.Blackboard;
            var stats = Get<StatSystem>();

            if (blackboard.DominationLifeTime > 0)
            {
                blackboard.DominationLifeTime--;
            }
            if (blackboard.DominationLifeTime > 0 || game.Turn.Value < 21)
            {
                return;
            }

            blackboard.Domination = Get<DiplomacySystem>().GetAvgStatFromAllCities(StatCategory.Population) / stats.Population.MaxValue.Value;
            if (blackboard.Domination < 2)
            {
                return;
            }

            blackboard.DominationLifeTime = 10;
            ShowEventInQueue("Domination");
        }

        public void ShowBattleResult(GameEvent model, BattleResult result)
        {
            var gameEvent = FindContext(model);
            gameEvent.Model.ShowResult(out string title, out string text);

            _eventBattleResult.Attach(gameEvent);
            _eventBattleResult.Show(title, text, result);

            Get<SoundSystem>().PlayBattleSound();
        }

        public void ShowResultInQueue(GameEventContext gameEvent)
        {
            if (EventDialogOpened())
            {
                gameEvent.QueueMode = EventQueueMode.ShowResult;
                _eventQueue.Enqueue(gameEvent);

                return;
            }

            // Show result dialog
            gameEvent.Model.ShowResult(out string title, out string text);

            _eventDialog.Attach(gameEvent);
            _eventDialog.Show(title, text);

            // End event effects
            EndEvent(gameEvent);

            if (gameEvent.Model.IsBattleEvent)
            {
                Get<SoundSystem>().PlayBattleSound();
            }
        }
        
        /// <summary>
        /// Нормальное завершение события (с эффектом завершения), которое было добавлено в общий список активных событий (с иконкой-эффектом)
        /// </summary>
        /// <param name="gameEvent"></param>
        public void EndEvent(GameEventContext gameEvent)
        {
            gameEvent.Model.EndEffect();
            gameEvent.Completed.Value = true;
            gameEvent.DetachAll();

            GameState.Current.Events.Remove(gameEvent);

            // Персидское вторжение, закончилось по длительности, сохраняем ссылку на контекст для выбора тактики сражения
            if (gameEvent.Model.Id == "33")
            {
                _nonEffectEvents.Add(gameEvent);
            }
        }

        public bool EventDialogOpened()
        {
            return _finalView.gameObject.activeSelf ||
                   _unionView.gameObject.activeSelf ||
                _eventDialog.gameObject.activeSelf ||
                _eventBigDialog.gameObject.activeSelf ||
                _eventBattle.gameObject.activeSelf ||
                _eventBattleResult.gameObject.activeSelf;
        }

        /// <summary>
        /// Update event queue after result event dialog closed
        /// </summary>
        public void UpdateEventQueue()
        {
            if (EventDialogOpened() || _eventQueue.Count == 0)
            {
                return;
            }

            var gameEvent = _eventQueue.Dequeue();

            if (gameEvent.QueueMode == EventQueueMode.None)
            {
                Debug.LogError($"Missing event queue mode: {gameEvent.Model.Id}");
            }
            if (gameEvent.QueueMode == EventQueueMode.RunEvent)
            {
                RunEventInQueue(gameEvent);
            }
            if (gameEvent.QueueMode == EventQueueMode.ShowEvent)
            {
                ShowEventInQueue(gameEvent);
            }
            if (gameEvent.QueueMode == EventQueueMode.ShowResult)
            {
                ShowResultInQueue(gameEvent);
            }
        }

        /// <summary>
        /// Завершает событие досрочно (без эффекта завершения)
        /// </summary>
        /// <param name="model"></param>
        public void AbortEvent(GameEvent model)
        {
            var ctx = GameSystem.Get<GameEventSystem>().FindContext(model);
            if (ctx == null)
            {
                return;
            }

            var duration = ctx.Duration.Value;
            if (duration > 0)
            {
                ctx.Completed.Value = true;
                ctx.DetachAll();

                GameState.Current.Events.Remove(ctx);
            }

            DestroyEvent(ctx);
        }

        public void UnlockWorldMap(bool isBezantia)
        {
            Get<DiplomacySystem>().StopAllTrades();

            GameState.Current.GameContext.OpenedMapId.Value = 2;

            GameState.Current.Cities.Clear();
            Get<DiplomacySystem>().CreateCities();

            GameState.Current.Regions.Clear();
            Get<RegionSystem>().CreateRegions();

            Get<DiplomacySystem>().SetupNewGame();
            Get<RegionSystem>().SetupNewGame();
            
            Get<DiplomacySystem>().UnlockWorldCities();

            Get<StatSystem>().ResetStatsForWorld();

            GameSystem.Get<AchievementsSystem>().Unlock("achiv5ach");

            if (isBezantia)
            {
                if (IsEventStarted("PersianVictory"))
                {
                    EndEvent(FindContext(FindEventModel("PersianVictory")));
                }

                GameState.Current.GameContext.IsBizantia.Value = true;
                GameState.Current.GameContext.HasBizantiaBonus.Value = true;

                ShowEventInQueue("NewEmpireBezantia");

                if (GameState.Current.GameContext.Model.CityType == CityType.Culture)
                {
                    RunEventInQueue("TradePartners");
                }
                if (GameState.Current.GameContext.Model.CityType == CityType.War)
                {
                    RunEventInQueue("RaidForSlaves");
                }
                if (GameState.Current.GameContext.Model.CityType == CityType.Trade)
                {
                    RunEventInQueue("RiskyInvestments");
                }
            }
            else
            {
                if (GameState.Current.GameContext.Model.CityType == CityType.Culture)
                {
                    ShowEventInQueue("NewEmpireAthens");
                    RunEventInQueue("TradePartners");
                }

                if (GameState.Current.GameContext.Model.CityType == CityType.War)
                {
                    ShowEventInQueue("NewEmpireSparta");
                    RunEventInQueue("RaidForSlaves");
                }

                if (GameState.Current.GameContext.Model.CityType == CityType.Trade)
                {
                    ShowEventInQueue("NewEmpireCorinth");
                    RunEventInQueue("RiskyInvestments");
                }
            }
        }

        public bool IsPersianWar => false;
        public int PersianWarStage = 0;
    }
}
