namespace Assets.Game.Domain.Models
{
    using System;
    using Systems;
    using UI;
    using UnityEngine;

//    [CreateAssetMenu(fileName = "GameEvent", menuName = "Data/GameEvent")]
    public abstract class GameEvent : ScriptableObject
    {
        public string Id;

        public Sprite Icon;
        public Sprite Background;

        public int Duration;
        public bool IsBattleEvent;

        public EventType EventType;
        public bool EventComplete;

        /// <summary>
        /// Show dialog after event started (automatically)
        /// </summary>
        /// <param name="text"></param>
        /// <param name="actions"></param>
        /// <param name="title"></param>
        public virtual bool ShowDialog(out string title, out string text)
        {
            title = name;
            text = string.Empty;
            
            return false; // �� ���������� ������
        }

        /// <summary>
        /// Show dialog after event icon clicked
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="actions"></param>
        public virtual void ShowEffect(out string title, out string text, out Action<EventActionView>[] actions)
        {
            title = name;
            text = string.Empty;
            actions = null;
        }

        /// <summary>
        /// Show dialog after event finished
        /// </summary>
        /// <param name="title"></param>
        public virtual void ShowResult(out string title, out string text)
        {
            title = name;
            text = string.Empty;
        }

        /// <summary>
        /// Run/apply effect of event
        /// </summary>
        public virtual void RunEffect(){}

        /// <summary>
        /// Can be event finished
        /// </summary>
        /// <returns></returns>
        public virtual bool CanFinish()
        {
            return false;
        }

        public virtual void UpdateEventEffect()
        {
        }

        /// <summary>
        /// Apply rewards/end event effects
        /// </summary>
        public virtual void EndEffect() { }

        /// <summary>
        /// Resolve player actions on event dialog
        /// </summary>
        /// <param name="index"></param>
        public virtual void ResolveAction(int index) { }

        /// <summary>
        /// Play animations for event
        /// </summary>
        /// <param name="state"></param>
        public virtual void PlayAnimation(int state) { }

        public virtual bool CanCloseEvent()
        {
            return true;
        }
        public virtual bool ShowEffectOnStart()
        {
            return false;
        }

        /// <summary>
        /// �������� �� ��������� � ������� ������� �������. �� ������� ���������
        /// </summary>
        /// <returns></returns>
        public virtual string SubmitAction()
        {
            return string.Empty; // �������� ������ ��������
        }
        public virtual bool SubmitEnabled()
        {
            return false; // �������� ��� ��� ��������
        }
    }

//    (backType == 1) "effectBack"
//    (backType == 2) "effectGold"
//    (backType == 3) "effecBlood"
//    (backType == 4) "effecSilver"
//    (backType == 5) "effecIron"
//    (backType == 6) "effecCopper"

    public enum EventType
    {
        Neutral,
        Positive,
        Negative,
        Trial,
        None
    }
}