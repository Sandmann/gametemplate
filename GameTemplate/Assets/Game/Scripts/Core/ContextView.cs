﻿namespace Assets.Game.Core.Common
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IContextView
    {
        void Attach(IContext context);
        void Dispose();
        void MarkDirty();
        void UpdateState();
    }

    public abstract class View
    {
        private static readonly Dictionary<string, IContextView> Views = new Dictionary<string, IContextView>();
        
        public static void Add(IContextView view)
        {
            Views[view.GetType().Name] = view;
        }

        public static void Remove(IContextView view)
        {
            if (Views.ContainsKey(view.GetType().Name))
            {
                Views.Remove(view.GetType().Name);
            }
        }

        public static T Get<T>() where T : IContextView
        {
            var typeName = typeof(T).FullName;
            if (typeName == null || !Views.ContainsKey(typeName))
            {
                Debug.LogWarning($"View is missing: {typeName}");
                return default;
            }

            return (T)Views[typeName];
        }
    }

    public abstract class ContextView<T> : MonoBehaviour, IContextView where T : class, IContext
    {
        private T _context;
        public T GetContext() { return _context; }

        protected bool IsDirty;
        public void MarkDirty()
        {
            IsDirty = true;

            // Ability to update state if inactive
            if (gameObject && !gameObject.activeSelf)
                UpdateState();
        }

        protected virtual void Update()
        {
            if (IsDirty)
            {
                UpdateState();
                IsDirty = false;
            }
        }

        protected void OnDestroy()
        {
            _context?.DetachView(this);
        }

        public void UpdateState()
        {
            if (GetContext() != null)
                OnUpdate();
        }

        /// <summary>
        /// Attach view to context
        /// </summary>
        /// <param name="context"></param>
        public void Attach(IContext context)
        {
            Detach();

            _context = (T)context;

            _context.AttachView(this);
            
            OnAttach();
            MarkDirty();
        }

        /// <summary>
        /// Detach view from its context
        /// </summary>
        public void Detach()
        {
            if (_context != null)
            {
                _context.DetachView(this);

                Dispose();
                _context = null;
            }                        
        }

        public virtual void Dispose()
        {
        }

        /// <summary>
        /// Will call every time when context notify it
        /// </summary>
        protected virtual void OnUpdate() { }
        
        /// <summary>
        /// Calls once after binding
        /// </summary>
        protected virtual void OnAttach() { }
    }
}