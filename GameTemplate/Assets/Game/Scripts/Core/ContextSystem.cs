﻿namespace Assets.Game.Core.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using UnityEngine;

    public abstract class ContextSystem
    {
        private static Dictionary<string, ContextSystem> _systems = new Dictionary<string, ContextSystem>();

        public static T Create<T>() where T : ContextSystem, new()
        {
            var sys = new T();

            Add(sys);
            return sys;
        }

        public static void Add(ContextSystem sys)
        {
            _systems[sys.GetType().FullName] = sys;
        }

        public static T Get<T>() where T : ContextSystem
        {
            var typeName = typeof(T).FullName;
            if (typeName == null || !_systems.ContainsKey(typeName))
            {
                Debug.LogWarning($"System is missing: {typeName}");
                return default;
            }

            return _systems[typeName] as T;
        }

        public virtual async Task UpdateAsync()
        {
            await Task.Yield();
        }

        public virtual async Task SetupAsync()
        {
            await Task.Yield();
        }
    }
}
