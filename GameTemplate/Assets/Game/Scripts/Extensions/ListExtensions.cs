﻿namespace Assets.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T GetRandom<T>(this IList<T> list)
        {
            var copy = list.ToArray();
            for (int i = 0; i < 10; i++)
            {
                copy.Shuffle();
            }

            return copy.FirstOrDefault();
        }
    }
}
