﻿namespace Assets.Extensions
{
    using UnityEngine;

    public static class TransformExtension
    {
        public static void Reset(this Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
            t.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Release all children of transform through ObjectPool
        /// (If pool is not created object destroyed)
        /// </summary>
        /// <param name="t"></param>
        public static void DestroyChildren(this Transform t)
        {
            while (t.childCount > 0)
            {
                var obj = t.GetChild(0);
                obj.gameObject.SetActive(false);
                obj.SetParent(null);
                GameObject.Destroy(obj.gameObject);
            }
        }
    }
}
