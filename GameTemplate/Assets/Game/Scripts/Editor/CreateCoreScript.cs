﻿namespace Assets.Game.Core.Editor
{
    using UnityEditor;

    public class CreateCoreScript
    {
        [MenuItem(itemName: "Assets/Create/Core Script/Context System", isValidateFunction: false, priority: 51)]
        public static void CreateGameSystemFromTemplate()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile("Assets/Game/Scripts/Editor/Templates/ContextSystemTemplate.cs.txt", "NewContextSystem.cs");
        }

        [MenuItem(itemName: "Assets/Create/Core Script/Model", isValidateFunction: false, priority: 51)]
        public static void CreateModelFromTemplate()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile("Assets/Game/Scripts/Editor/Templates/ModelTemplate.cs.txt", "NewModel.cs");
        }

        [MenuItem(itemName: "Assets/Create/Core Script/Context", isValidateFunction: false, priority: 51)]
        public static void CreateContextFromTemplate()
        {
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile("Assets/Game/Scripts/Editor/Templates/ContextTemplate.cs.txt", "NewContext.cs");
        }
    }
}
