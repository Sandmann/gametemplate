﻿namespace Assets.Game.Domain
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Contexts;
    using Core.Common;
    using Models;
    using UnityEngine;
    using UnityEngine.AddressableAssets;

    [Serializable]
    public class GameState
    {
        [SerializeField]
        private uint _seed = 1; // seed for context ids

        #region Game Constants

        public const string Version = "0.1";

        #endregion
        
        private static GameState _instance;
        public static GameState Current
        {
            get { return _instance; }
            set { _instance = value; }
        }

        public GameContext GameContext = null;
        public LocalizationContext LocalizationContext = null;

        #region Context collection

        // TODO: Add there game data
        //public List<GameContext> Items = new List<GameContext>();

        #endregion

        public TContext CreateContext<TContext, TModel>(TModel model) where TContext : Context, new() where TModel: ScriptableObject
        {
            var ctx = (Context<TModel>)(object)CreateContext<TContext>();
            ctx.Model = model;

            return (TContext)(object)ctx;
        }

        public async Task<TContext> CreateContextAsync<TContext, TModel>(string assetId) where TContext : Context, new() where TModel: ScriptableObject
        {
            var handle = Addressables.LoadAssetAsync<TModel>(assetId);
            await handle.Task;

            var ctx = (Context<TModel>)(object)CreateContext<TContext>();
            ctx.Model = handle.Result;

            return (TContext)(object)ctx;
        }
        
        public async Task CreateContextAllAsync<TContext, TModel>(string groupLabel) where TContext : Context, new() where TModel: ScriptableObject
        {
            var handle = Addressables.LoadAssetsAsync<TModel>(groupLabel, (model) =>
            {
                var ctx = (Context<TModel>)(object)CreateContext<TContext>();
                ctx.Model = model;
            });

            await handle.Task;
        }

        public T CreateContext<T>() where T : Context, new()
        {
            var ctx = new T()
            {
                Id = _seed++,
            };

            // Add context into State
            var fields = GetType().GetFields();
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i].FieldType.IsGenericType && fields[i].FieldType.GetGenericArguments()[0] == typeof(T))
                {
                    ((IList) fields[i].GetValue(this)).Add(ctx);
                }

                if (fields[i].FieldType == typeof(T))
                {
                    fields[i].SetValue(this, ctx);
                }
            }

            return ctx;
        }

        public async Task<T> LoadModelAsync<T>(string assetId) where T: ScriptableObject
        {
            var handle = Addressables.LoadAssetAsync<T>(assetId);
            await handle.Task;

            return handle.Result;
        }

        public async Task<IList<T>> LoadModelAllAsync<T>(string groupId) where T: ScriptableObject
        {
            var handle = Addressables.LoadAssetsAsync<T>(groupId, o => {});
            await handle.Task;

            return handle.Result;
        }
    }
}
