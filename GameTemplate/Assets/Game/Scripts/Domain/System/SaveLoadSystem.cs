﻿namespace Assets.Game.Domain.Systems
{
    using System;
    using System.IO;
    using System.Text;
    using Core.Common;
    using UnityEngine;

    [Serializable]
    public class SaveLoadSystem : ContextSystem
    {
        public void SaveGame(string fileName = "save")
        {
            Debug.Log("Saving...");

            var saveFile = GameState.Current;            

            var path = Path.Combine(Application.dataPath, fileName + ".bin");

            var text = JsonUtility.ToJson(saveFile);
            File.WriteAllBytes(path, Encoding.UTF8.GetBytes(text));

            Debug.Log("Saved");            
        }

        public void LoadGame(string fileName)
        {
//            SceneLoadController.Instance.Show(true);

            Debug.Log("Loading...");

            var path = Path.Combine(Application.dataPath, fileName + ".bin");
            if (!File.Exists(path))
            {
                Debug.LogError("File not found: " + path);
                return;
            }

            var text = Encoding.UTF8.GetString(File.ReadAllBytes(path));
            var saveFile = JsonUtility.FromJson<GameState>(text);

            GameState.Current = saveFile;
            

            Debug.Log("Loaded");
        }
    }


}
