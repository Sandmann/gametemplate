﻿namespace Assets.Game.Domain
{
    using System.Linq;
    using System.Threading.Tasks;
    using Systems;
    using Contexts;
    using UnityEngine;
    using Models;
    using TMPro;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    using Assets.Game.Core.Common;
    using Assets.Game.Scripts.Domain.View.Screens;
    using UnityEngine.Profiling;
    using System.Collections;
    using System.Collections.Generic;

    public class GameScene : MonoBehaviour
    {
        public static GameScene Instance { get; private set; }

#pragma warning disable 649
        [Header("Systems")]
        [SerializeField] private SaveLoadSystem _saveLoadSystem;

        [Header("Models")]
        [SerializeField] private GameModel _defaultGameModel;

        [Header("Views")]
        [SerializeField] private GameObject _lockScreen;
        [SerializeField] private InGameMenuView _inGameMenuView;
#pragma warning restore 649

        private LocalizationSystem _localization;

        public async void Start()
        {
            if (Instance != null)
            {
                Destroy(Instance.gameObject);
            }
            
            Instance = this;

            // Создаем новое состояние игры
            var isNewGame = GameState.Current == null;
            if (isNewGame)
            {
                GameState.Current = new GameState();
                GameState.Current.CreateContext<GameContext, GameModel>(_defaultGameModel);
            }

            // Загружаем общие игровые системы. Были созданы снаружи сцены
            _localization = ContextSystem.Get<LocalizationSystem>();

            // Добавляем все системы игровой сцены
            ContextSystem.Add(_saveLoadSystem);

            // Инициализация новой игры
            if (isNewGame)
            {
                await _localization.SetupAsync();

                await SetupNewGame();
            }           

            // Инициализация GUI
            _inGameMenuView.Setup();

            // настроить кнопки навигации по экранам

            Debug.Log("Scene initialized");
            LockScreen(false);
        }

        private async Task SetupNewGame()
        {
            var game = GameState.Current.GameContext;

            // TODO: Initialize setup state for new game

            await Task.Yield();
        }

        private void LockScreen(bool value)
        {
            if (value)
            {
                _lockScreen.SetActive(true);
            }
            else
            {
                _lockScreen.SetActive(false);
            }
        }

        protected void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                _inGameMenuView.ShowOrHide();
            }

            //if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            //{
            //}
        }
    }
}
