namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;
    using UnityEngine;

    [Serializable]
    public class GameContext : Context<GameModel>
    {
    }
}
