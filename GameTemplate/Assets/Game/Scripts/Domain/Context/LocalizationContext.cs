namespace Assets.Game.Domain.Contexts
{
    using System;
    using Core.Common;
    using Models;
    using UnityEngine;

    [Serializable]
    public class LocalizationContext : Context<LocalizationModel>
    {
        [SerializeField] private int _curLanguage;

        public LanguageInfo CurrentLanguage
        {
            get { return _curLanguage < Model.Languages.Length ? Model.Languages[_curLanguage] : default; }
            set
            {
                _curLanguage = Array.IndexOf(Model.Languages, value);
            }
        }
    }
}
