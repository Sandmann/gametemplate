namespace Assets.Game.Domain.Models
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "GameModel", menuName = "Data/GameModel")]
    public class GameModel : ScriptableObject
    {
        public string Id;
    }
}
