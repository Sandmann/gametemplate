﻿
namespace Assets.Game.Domain.Systems
{
    using System.Collections;
    using System.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class LoadSceneView : MonoBehaviour
    {
        public float FadeDuration = 1f;
        public float LoadDuration = 1f;

#pragma warning disable 649

        [SerializeField] private Image _progressBar;
        [SerializeField] private Text _progressBarText;

        [SerializeField] private CanvasGroup _canvasGroup;

#pragma warning restore 649               
        
        private float _progress = 0f;
        private bool _isClicked = false;

        private static LoadSceneView _instance;
        public static LoadSceneView Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                _instance = FindObjectOfType<LoadSceneView>();
                if (_instance != null)
                {
                    return _instance;
                }

                var prefab = Resources.Load<LoadSceneView>("Prefabs/SceneLoader");
                _instance = Instantiate(prefab);

                return _instance;
            }
        }

        protected void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }

        public void Show(bool faded = false)
        {
            gameObject.SetActive(true);

            _isClicked = false;
            _progress = 0f;

            UpdateProgressBar();

            _canvasGroup.blocksRaycasts = true;
            _canvasGroup.alpha = faded ? 0f : 1f;
            _canvasGroup.gameObject.SetActive(true);
        }
        
        public async Task Load(string sceneName = null)
        {
            LoadDuration = 2f;

            if (sceneName == null)
            {
#if UNITY_EDITOR
                LoadDuration = (SceneManager.GetActiveScene().name == "MenuScene") ? 2f : 2f;
#endif

                await LoadEmptyAsync();
            }
            else
            {
#if UNITY_EDITOR
                LoadDuration = (sceneName == "MenuScene") ? 2f : 3f;
#endif
                
                await LoadAsync(sceneName);
            }
        }

        private async Task FadeToAsync(float alpha)
        {
            float fadeSpeed = Mathf.Abs(_canvasGroup.alpha - alpha) / FadeDuration;
            while (!Mathf.Approximately(_canvasGroup.alpha, alpha))
            {
                _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, alpha,
                    fadeSpeed * Time.deltaTime);
                
                await Task.Yield();
            }
        }

        private async Task LoadEmptyAsync()
        {   
            var elapsedTime = 0f;
            while (_progress < 1f)
            {
                _progress = Mathf.Clamp01(elapsedTime / LoadDuration);
                elapsedTime += Time.deltaTime;

                UpdateProgressBar();
                await Task.Yield();
            }

            _progressBarText.text = "Click anywhere to start.";

            while (!_isClicked)
            {
                await Task.Yield();
            }

            await ShowSceneAsync();
        }

        private async Task LoadAsync(string sceneName)
        {
            Debug.Log("Start load");

            await FadeToAsync(1f);

            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
            operation.allowSceneActivation = true;

            var elapsedTime = 0f;
            while (_progress < 1f)
            {
                _progress = Mathf.Clamp01(elapsedTime * operation.progress / (0.9f * LoadDuration));
                elapsedTime += Time.deltaTime;

                UpdateProgressBar();
                await Task.Yield();
            }

            Debug.Log($"Complete load: {operation.isDone}");
            _progressBarText.text = "Click anywhere to start.";

            while (!_isClicked)
            {
                await Task.Yield();
            }

            operation.allowSceneActivation = true;
            Debug.Log($"Activate load: {operation.isDone}");

            // Задержка на выполнение старта сцены
//            await Task.Delay(1000);
            await ShowSceneAsync();
        }

        private void UpdateProgressBar()
        {
            _progressBar.fillAmount = _progress;
            _progressBarText.text = $"Loading... {(int)(_progress * 100f)}%";
        }

        public async Task ShowSceneAsync()
        {
            await Task.Delay(100);
            await FadeToAsync(0f);
            
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.gameObject.SetActive(false);            
            gameObject.SetActive(false);

            Debug.Log("Scene on screen and ready");
        }

        protected void Update()
        {
            if (Input.GetMouseButtonUp(0) && _progress >= 1f)
            {
                _isClicked = true;
            }
        }
    }
}
