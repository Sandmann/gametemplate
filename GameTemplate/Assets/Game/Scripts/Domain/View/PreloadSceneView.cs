﻿namespace Assets.Game.Domain.Systems
{
    using Core.Common;
    using System.Threading.Tasks;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class PreloadSceneView
    {
#pragma warning disable 649
#pragma warning restore 649

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void InitializeBeforeSceneLoad()
        {
            Debug.Log("[Preload] Before scene load");

            #region Create common systems shared with all scenes (eg. localization, sounds, anims)                       

            ContextSystem.Create<LocalizationSystem>();

            #endregion

            SceneManager.LoadScene("Loading", LoadSceneMode.Additive);            
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitializeAfterSceneLoad()
        {
            Debug.Log("[Preload] After scene load");
            
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public static async void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            Debug.Log("[Preload] Scene loaded");

            LoadSceneView.Instance.Show();
            await LoadSceneView.Instance.Load();

            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}
