namespace Assets.Game.Scripts.Domain.View.Screens
{
    using Assets.Game.Core.Common;
    using Assets.Game.Domain.Systems;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class InGameMenuView : MonoBehaviour
    {
#pragma warning disable 649

        [SerializeField] private TextMeshProUGUI _title;

        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _saveGameButton;
        [SerializeField] private Button _loadGameButton;
        [SerializeField] private Button _exitGameButton;

#pragma warning restore 649

        public void Setup()
        {
            _title.text = ContextSystem.Get<LocalizationSystem>().Get("ui_ingamemenu_title");

            _resumeButton.onClick.RemoveAllListeners();
            _resumeButton.onClick.AddListener(() => Resume());

            _saveGameButton.onClick.RemoveAllListeners();
            _saveGameButton.onClick.AddListener(() => SaveGame());

            _loadGameButton.onClick.RemoveAllListeners();
            _loadGameButton.onClick.AddListener(() => LoadGame());

            _exitGameButton.onClick.RemoveAllListeners();
            _exitGameButton.onClick.AddListener(() => ExitGame());

            gameObject.SetActive(false);
        }

        public void ShowOrHide()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }

        private void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void SaveGame()
        {
            ContextSystem.Get<SaveLoadSystem>().SaveGame();            
        }

        private void Resume()
        {
            ShowOrHide(); 
        }

        private async void LoadGame()
        {
            ContextSystem.Get<SaveLoadSystem>().LoadGame("save");

            LoadSceneView.Instance.Show(true);
            await LoadSceneView.Instance.Load("GameScene");
        }

        //private async void Reload()
        //{
        //    GameState.Current = null;

        //    LoadSceneView.Instance.Show(true);
        //    await LoadSceneView.Instance.Load("GameScene");
        //}
    }
}